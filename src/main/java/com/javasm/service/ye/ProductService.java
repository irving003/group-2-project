package com.javasm.service.ye;

import com.javasm.entity.ye.YProduct;
import com.javasm.service.base.BaseService;

import java.util.List;

public interface ProductService extends BaseService<YProduct> {
    //产品动态条件查询
    List<YProduct> findYProduct(YProduct yProduct);

    //根据编号查询
    YProduct findByCoding(long coding);

}

package com.javasm.service.ye;

import com.javasm.entity.ye.YBinding;
import com.javasm.service.base.BaseService;

import java.util.List;

public interface BindingService extends BaseService<YBinding> {
    //动态条件查询
    List<YBinding> findYBinding(YBinding yBinding);
}

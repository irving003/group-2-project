package com.javasm.service.ye.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.javasm.entity.ye.YType;
import com.javasm.mapper.ye.TypeMapper;
import com.javasm.service.ye.TypeService;
import com.javasm.service.base.base.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class TypeServiceImpl extends BaseServiceImpl<YType> implements TypeService {
    @Autowired
    private TypeMapper typeMapper;

    @Override
    public List<YType> queryName(String name) {
        LambdaQueryWrapper<YType> lambda = new LambdaQueryWrapper<YType>();
        // 设置查询条件
        lambda.like(YType::getGoodsName,name);
        List<YType> yTypes = typeMapper.selectList(lambda);
        return yTypes;
    }
}

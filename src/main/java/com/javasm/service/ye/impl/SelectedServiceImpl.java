package com.javasm.service.ye.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.javasm.entity.ye.Brand;
import com.javasm.entity.ye.YSelected;
import com.javasm.mapper.ye.SelectedMapper;
import com.javasm.service.ye.BrandService;
import com.javasm.service.ye.SelectedService;
import com.javasm.service.base.base.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SelectedServiceImpl extends BaseServiceImpl<YSelected> implements SelectedService {
    @Autowired
    private SelectedMapper selectedMapper;

    @Autowired
    private BrandService brandService;

    @Override
    public List<Brand> queryById(long typeId) {
        // 获取lambda的查询条件对象
        LambdaQueryWrapper<YSelected> lambda = new LambdaQueryWrapper<>();
        // 设置查询条件
        // User::getName 使用了函数式编程接口
        lambda.eq(YSelected::getTypeId,typeId);
        List<YSelected> ySelecteds = selectedMapper.selectList(lambda);
        List<Brand> brands = new ArrayList<>();
        for (YSelected id : ySelecteds) {
            brands.add(brandService.findById(id.getBrandId()));

        }
        return brands;
    }
}

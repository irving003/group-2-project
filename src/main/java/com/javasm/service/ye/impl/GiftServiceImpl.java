package com.javasm.service.ye.impl;

import com.javasm.entity.ye.YGift;
import com.javasm.entity.ye.YGoods;
import com.javasm.mapper.ye.GiftMapper;
import com.javasm.service.base.base.BaseServiceImpl;
import com.javasm.service.ye.GiftService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GiftServiceImpl extends BaseServiceImpl<YGift> implements GiftService {
    @Autowired
    private GiftMapper giftMapper;

    @Override
    public List<YGift> findYGift(YGift yGift) {
        List<YGift> yGift1 = giftMapper.findYGift(yGift);
        return yGift1;
    }

    // 商品转赠品动态条件查询
    @Override
    public List<YGift> findYGoodsAndYGift(YGift yGift) {
        List<YGift> yGift2 = giftMapper.findYGoodsAndYGift(yGift);
        return yGift2;
    }


}

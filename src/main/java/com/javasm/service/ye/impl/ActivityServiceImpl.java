package com.javasm.service.ye.impl;

import com.javasm.entity.ye.YActivity;
import com.javasm.mapper.ye.ActivityMapper;
import com.javasm.service.base.base.BaseServiceImpl;
import com.javasm.service.ye.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActivityServiceImpl extends BaseServiceImpl<YActivity> implements ActivityService {
   @Autowired
   private ActivityMapper mapper;

    @Override
    public List<YActivity> conditionQuery(YActivity activity) {
        List<YActivity> yActivities = mapper.conditionQuery(activity);
        return yActivities;
    }
}

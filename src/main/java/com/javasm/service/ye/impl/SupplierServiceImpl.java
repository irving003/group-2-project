package com.javasm.service.ye.impl;

import com.javasm.entity.ye.YSupplier;
import com.javasm.mapper.ye.SupplierMapper;
import com.javasm.service.ye.SupplierService;
import com.javasm.service.base.base.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SupplierServiceImpl extends BaseServiceImpl<YSupplier> implements SupplierService {
    @Autowired
    private SupplierMapper supplierMapper;

    @Override
    public List<YSupplier> findYSupplier(YSupplier ySupplier) {
        List<YSupplier> ySupplier1 = supplierMapper.findYSupplier(ySupplier);
        return ySupplier1;
    }
}

package com.javasm.service.ye.impl;

import com.javasm.entity.ye.YTariff;
import com.javasm.mapper.ye.TariffMapper;
import com.javasm.service.ye.TariffService;
import com.javasm.service.base.base.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TariffServiceImpl extends BaseServiceImpl<YTariff> implements TariffService {
    @Autowired
    private TariffMapper tariffMapper;

    @Override
    public List<YTariff> findYTariff(YTariff yTariff) {
        List<YTariff> yTariff1 = tariffMapper.findYTariff(yTariff);
        return yTariff1;
    }
}

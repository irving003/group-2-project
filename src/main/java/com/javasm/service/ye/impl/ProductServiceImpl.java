package com.javasm.service.ye.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.javasm.entity.ye.YGoods;
import com.javasm.entity.ye.YProduct;
import com.javasm.mapper.ye.ProductMapper;
import com.javasm.service.ye.ProductService;
import com.javasm.service.base.base.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl  extends BaseServiceImpl<YProduct> implements ProductService{
    @Autowired
    private ProductMapper productMapper;


    @Override
    public List<YProduct> findYProduct(YProduct yProduct) {
        List<YProduct> yProduct1 = productMapper.findYProduct(yProduct);
        return yProduct1;
    }

    @Override
    public YProduct findByCoding(long coding) {
        // 获取lambda的查询条件对象
        LambdaQueryWrapper<YProduct> lambda = new LambdaQueryWrapper<>();
        lambda.eq(YProduct::getCoding, coding);
        YProduct product = productMapper.selectOne(lambda);
        return product;
    }
}

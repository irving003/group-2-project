package com.javasm.service.ye.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.javasm.entity.ye.YGoods;
import com.javasm.mapper.ye.YGoodsMapper;
import com.javasm.service.ye.YGoodsService;
import com.javasm.service.base.base.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class YGoodsServiceImpl extends BaseServiceImpl<YGoods> implements YGoodsService {
    @Autowired
    private YGoodsMapper yGoodsMapper;


    @Override
    public List<YGoods> findYGoods(YGoods yGoods) {
        List<YGoods> yGoods1 = yGoodsMapper.findYGoods(yGoods);
        return yGoods1;
    }

    @Override
    public List<YGoods> queryGrade(long grade) {
        // 获取lambda的查询条件对象
        LambdaQueryWrapper<YGoods> lambda = new LambdaQueryWrapper<>();
        // 设置查询条件
        // User::getName 使用了函数式编程接口
        lambda.eq(YGoods::getGoodsGrade,grade);
        List<YGoods> yGoods = yGoodsMapper.selectList(lambda);
        return yGoods;
    }

    @Override
    public List<YGoods> findYGiftAndYGoods(YGoods yGoods) {
        List<YGoods> yGoods2 = yGoodsMapper.findYGiftAndYGoods(yGoods);
        return yGoods2;
    }

    @Override
    public int deletes(List<Long> l) {
        int i = yGoodsMapper.deleteBatchIds(l);
        return i;
    }


}

package com.javasm.service.ye.impl;

import com.javasm.entity.ye.Brand;
import com.javasm.service.ye.BrandService;
import com.javasm.service.base.base.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @author: Author
 * @className: BrandServiceImpl
 * @description:
 * @date: 2023/6/5 11:31
 * @since: 11
 */
@Service
public class BrandServiceImpl extends BaseServiceImpl<Brand> implements BrandService {

}

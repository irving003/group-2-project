package com.javasm.service.ye.impl;

import com.javasm.entity.ye.YBinding;
import com.javasm.mapper.ye.BindingMapper;
import com.javasm.service.base.base.BaseServiceImpl;
import com.javasm.service.ye.BindingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class BindingServiceImpl extends BaseServiceImpl<YBinding> implements BindingService {
    @Autowired
    private BindingMapper bindingMapper;


    @Override
    public List<YBinding> findYBinding(YBinding yBinding) {
        List<YBinding> yBinding1 = bindingMapper.findYBinding(yBinding);
        return yBinding1;
    }
}

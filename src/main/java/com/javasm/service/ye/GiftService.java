package com.javasm.service.ye;

import com.javasm.entity.ye.YGift;
import com.javasm.entity.ye.YGoods;
import com.javasm.service.base.BaseService;

import java.util.List;

public interface GiftService extends BaseService<YGift> {
    //动态条件查询
    List<YGift> findYGift(YGift yGift);

    // 商品转赠品动态条件查询
    List<YGift> findYGoodsAndYGift(YGift yGift);
}

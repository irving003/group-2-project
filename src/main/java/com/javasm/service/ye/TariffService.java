package com.javasm.service.ye;

import com.javasm.entity.ye.YTariff;
import com.javasm.service.base.BaseService;

import java.util.List;

public interface TariffService extends BaseService<YTariff> {
    // 动态条件查询
    List<YTariff> findYTariff(YTariff yTariff);
}

package com.javasm.service.ye;

import com.javasm.entity.ye.YSelected;
import com.javasm.entity.ye.YType;
import com.javasm.service.base.BaseService;

import java.util.List;

public interface TypeService extends BaseService<YType> {
    //条件查询
    List<YType> queryName(String name);

}

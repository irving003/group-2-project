package com.javasm.service.ye;

import com.javasm.entity.ye.YGoods;
import com.javasm.service.base.BaseService;

import java.util.List;

public interface YGoodsService extends BaseService<YGoods> {


    //商品动态条件查询
    List<YGoods> findYGoods(YGoods yGoods);

    //一类商品/二类商品条件查询
    List<YGoods> queryGrade(long grade);

    // 赠品转商品动态条件查询
    List<YGoods> findYGiftAndYGoods(YGoods yGoods);

    //批量删除
    int deletes(List<Long> l);

}

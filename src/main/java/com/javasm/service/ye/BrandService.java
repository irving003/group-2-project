package com.javasm.service.ye;

import com.javasm.entity.ye.Brand;
import com.javasm.service.base.BaseService;

/**
 * @author: Author
 * @className: BrandService
 * @description:
 * @date: 2023/6/5 11:28
 * @since: 11
 */
public interface BrandService extends BaseService<Brand> {


}

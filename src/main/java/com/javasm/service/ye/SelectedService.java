package com.javasm.service.ye;

import com.javasm.entity.ye.Brand;
import com.javasm.entity.ye.YSelected;
import com.javasm.service.base.BaseService;

import java.util.List;

public interface SelectedService extends BaseService<YSelected> {
    // 根据商品类型id查询
    List<Brand> queryById(long typeId);
}

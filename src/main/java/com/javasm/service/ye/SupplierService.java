package com.javasm.service.ye;

import com.javasm.entity.ye.YSupplier;
import com.javasm.service.base.BaseService;

import java.util.List;

public interface SupplierService extends BaseService<YSupplier> {
    //供应商动态条件查询
    List<YSupplier> findYSupplier(YSupplier ySupplier);
}

package com.javasm.service.ye;

import com.javasm.entity.ye.YActivity;
import com.javasm.mapper.ye.ActivityMapper;
import com.javasm.service.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


public interface ActivityService extends BaseService<YActivity> {
    // 动态条件查询
    List<YActivity> conditionQuery(YActivity activity);
}

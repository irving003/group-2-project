package com.javasm.service.base.base;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.javasm.mapper.base.MyMapper;
import com.javasm.service.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author: Author
 * @className: BaseServiceImpl
 * @description:
 * @date: 2023/6/5 11:44
 * @since: 11
 */
public class BaseServiceImpl<T> implements BaseService<T> {

    @Autowired
    private MyMapper<T> myMapper;
    @Override
    public List<T> list() {
        return myMapper.selectList(null);
    }

    @Override
    public T findById(Long id) {
        return myMapper.selectById(id);
    }

    @Override
    public int add(T t) {
        return myMapper.insert(t);
    }

    @Override
    public int update(T t) {
        return myMapper.updateById(t);
    }

    @Override
    @Transactional
    public int deleteById(Long id) {
        return myMapper.deleteById(id);
    }

    @Override
    public int batchDeleteByIds(List<Long> ids) {
        return myMapper.deleteBatchIds(ids);
    }

    @Override
    public List<T> search(QueryWrapper<T> queryWrapper) {
        return myMapper.selectList(queryWrapper);
    }


}

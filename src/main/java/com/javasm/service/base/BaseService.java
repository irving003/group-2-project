package com.javasm.service.base;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import java.util.List;

/**
 * @author: Author
 * @className: BaseService
 * @description:
 * @date: 2023/6/5 11:42
 * @since: 11
 */
public interface BaseService<T> {

    /**
     * 查询所有
     */
    List<T> list();

    /**
     * 根据id查询
     */
    T findById(Long id);
    
    /**
     * 添加
     */
    int add(T t);

    /**
     * 修改
     */
    int update(T t);

    /**
     * 删除
     */
    int deleteById(Long id);

    /**
     * 批量删除
     */
    int batchDeleteByIds(List<Long> ids);

    /**
     * 条件查询
     */
    List<T> search(QueryWrapper<T> queryWrapper);

}

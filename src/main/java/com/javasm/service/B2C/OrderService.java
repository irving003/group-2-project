package com.javasm.service.B2C;

import com.javasm.entity.B2C.Order;
import com.javasm.entity.B2C.OrderQueryForm;
import com.javasm.service.base.BaseService;
import java.util.List;


public interface OrderService extends BaseService<Order> {

    List<Order> search(OrderQueryForm queryForm);

    Order findFormData(Long orderId);
}
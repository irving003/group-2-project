package com.javasm.service.B2C;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.javasm.entity.B2C.BConsignee;
import com.javasm.service.base.BaseService;

public interface ConsigneeService extends BaseService<BConsignee> {

    BConsignee findByOId(LambdaQueryWrapper<BConsignee> wrapper);
}

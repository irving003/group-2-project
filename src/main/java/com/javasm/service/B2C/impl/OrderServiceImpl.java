package com.javasm.service.B2C.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.javasm.common.page.PageBean;
import com.javasm.common.tools.SnowflakeIdWorker;
import com.javasm.entity.B2C.BConsignee;
import com.javasm.entity.B2C.BOrderCon;
import com.javasm.entity.B2C.Order;
import com.javasm.entity.B2C.OrderQueryForm;
import com.javasm.entity.ye.YGoods;
import com.javasm.mapper.B2C.ConsigneeMapper;
import com.javasm.mapper.B2C.OrderConMapper;
import com.javasm.mapper.B2C.OrderMapper;
import com.javasm.mapper.ye.YGoodsMapper;
import com.javasm.service.B2C.OrderService;
import com.javasm.service.base.base.BaseServiceImpl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class OrderServiceImpl extends BaseServiceImpl<Order> implements OrderService {

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private OrderConMapper orderConMapper;

    @Autowired
    private ConsigneeMapper consigneeMapper;
    @Autowired
    private YGoodsMapper yGoodsMapper;

    @Autowired
    private HttpSession session;

    @Override
    public List<Order> search(OrderQueryForm queryForm) {
        if (queryForm.getPageInfo() !=null) {
            PageHelper.startPage(queryForm.getPageInfo());
        }
        Long orderId = queryForm.getOrderId();
        String orderType = queryForm.getOrderType();
        String payType = queryForm.getPayType();
        String busType = queryForm.getBusType();
        LocalDateTime startTime = queryForm.getStartTime();
        String orderAction = queryForm.getOrderAction();
        LocalDateTime endTime = queryForm.getEndTime();
        String orderState = queryForm.getOrderState();
        LambdaQueryWrapper<Order> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(orderId !=null&&!"".equals(orderId), Order::getOrderId, orderId).
                eq(orderType !=null&&!"".equals(orderType)&&!"0".equals(orderType), Order::getOrderType, orderType).
                eq(payType !=null&&!"".equals(payType)&&!"0".equals(payType), Order::getPayType, payType).
                eq(busType !=null&&!"".equals(busType)&&!"0".equals(busType), Order::getBusType, busType).
                between(startTime !=null && endTime !=null, Order::getOrderTime, startTime, endTime).
                eq(orderAction !=null&&!"".equals(orderAction)&&!"0".equals(orderAction), Order::getOrderAction, orderAction).
                eq(orderState !=null&&!"".equals(orderState)&&!"0".equals(orderState), Order::getOrderState, orderState);
        List<Order> orders = orderMapper.selectList(wrapper);
        return orders;
    }

    @Override
    public Order findFormData(Long orderId) {
        LambdaQueryWrapper<BOrderCon> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(!String.valueOf(orderId).equals(""),BOrderCon::getOrderId,orderId);
        List<BOrderCon> bOrderCons = orderConMapper.selectList(wrapper);
        long congsigneeId = bOrderCons.get(0).getCongsigneeId();
        List<YGoods> goodsList = null;
        for (int i = 0; i < bOrderCons.size(); i++) {
           goodsList = new ArrayList<>();
           goodsList.add(yGoodsMapper.selectById(bOrderCons.get(i).getGoodsId())) ;
        }
        BConsignee bConsignee = consigneeMapper.selectById(congsigneeId);
        LambdaQueryWrapper<Order> wrapper1 = new LambdaQueryWrapper<>();
        wrapper1.eq(!String.valueOf(orderId).equals(""),Order::getOrderId,orderId);
        Order order = orderMapper.selectOne(wrapper1);
        order.setbConsignee(bConsignee);
        order.setGoods(goodsList);
        return order;
    }

    @Override
    public int update(Order order) {
        String loginName = (String) session.getAttribute("username");
        System.out.println("创建者"+loginName);
        order.setOutCreator(loginName);
        order.setCreateTime(LocalDateTime.now());
        SnowflakeIdWorker idWorker = new SnowflakeIdWorker(1,1);
        String id = String.valueOf(idWorker.nextId());
        order.setCouNum(id);
        return orderMapper.updateById(order);
    }

    @Override
    public int batchDeleteByIds(List<Long> ids) {
        for (Long id : ids) {
            Order order = orderMapper.selectById(id);
            Long orderId = order.getOrderId();
            LambdaQueryWrapper<BOrderCon> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(!String.valueOf(orderId).equals(""),BOrderCon::getOrderId,orderId);
            orderConMapper.delete(wrapper);
        }
        return orderMapper.deleteBatchIds(ids);
    }
}

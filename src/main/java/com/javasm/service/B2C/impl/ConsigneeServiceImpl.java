package com.javasm.service.B2C.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.javasm.entity.B2C.BConsignee;
import com.javasm.mapper.B2C.ConsigneeMapper;
import com.javasm.service.B2C.ConsigneeService;

import com.javasm.service.base.base.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConsigneeServiceImpl extends BaseServiceImpl<BConsignee> implements ConsigneeService {

    @Autowired
    private ConsigneeMapper consigneeMapper;

    @Override
    public BConsignee findByOId(LambdaQueryWrapper<BConsignee> wrapper) {
        BConsignee consignee = consigneeMapper.selectOne(wrapper);
        return consignee;
    }


}

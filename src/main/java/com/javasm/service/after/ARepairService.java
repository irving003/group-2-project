package com.javasm.service.after;

import com.javasm.entity.after.ARepair;
import com.javasm.service.base.BaseService;

public interface ARepairService extends BaseService<ARepair> {
}

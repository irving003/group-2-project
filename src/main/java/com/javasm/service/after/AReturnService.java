package com.javasm.service.after;

import com.javasm.entity.after.AReturn;
import com.javasm.service.base.BaseService;

public interface AReturnService extends BaseService<AReturn> {
}

package com.javasm.service.after;

import com.javasm.entity.after.ASaleImei;
import com.javasm.service.base.BaseService;

/**
 * @program: group-2-project
 * @description:
 * @author: lydms
 * @create: 2023-06-07 15:31
 **/

public interface ASaleImeiService extends BaseService<ASaleImei> {

        int deleteByOutCode(long id);
}

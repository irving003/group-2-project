package com.javasm.service.after.Impl;

import com.javasm.entity.after.ASale;

import com.javasm.service.after.ASaleService;
import com.javasm.service.base.base.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @program: group-2-project
 * @description:
 * @author: lydms
 * @create: 2023-06-06 09:34
 **/
@Service
public class ASaleServiceImpl extends BaseServiceImpl<ASale> implements ASaleService {

}

package com.javasm.service.after.Impl;

import com.javasm.entity.after.ARepair;
import com.javasm.service.after.ARepairService;
import com.javasm.service.base.base.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @program: group-2-project
 * @description:
 * @author: lydms
 * @create: 2023-06-07 00:10
 **/
@Service
public class ARepairServiceImpl extends BaseServiceImpl<ARepair> implements ARepairService {
}

package com.javasm.service.after.Impl;

import com.javasm.entity.after.AReturn;
import com.javasm.service.after.AReturnService;

import com.javasm.service.base.base.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @program: group-2-project
 * @description:
 * @author: lydms
 * @create: 2023-06-07 00:52
 **/
@Service
public class ARetrunServiceImpl extends BaseServiceImpl<AReturn> implements AReturnService {
}

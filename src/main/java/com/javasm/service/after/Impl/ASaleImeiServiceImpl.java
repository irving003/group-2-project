package com.javasm.service.after.Impl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.javasm.entity.after.ASaleImei;
import com.javasm.mapper.after.ASaleImeiMapper;
import com.javasm.service.after.ASaleImeiService;

import com.javasm.service.base.base.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * @program: group-2-project
 * @description:
 * @author: lydms
 * @create: 2023-06-07 15:34
 **/
@Service
public class ASaleImeiServiceImpl extends BaseServiceImpl<ASaleImei> implements ASaleImeiService {
    @Autowired
    private ASaleImeiMapper aSaleImeiMapper;

    @Override
    public int deleteByOutCode(long id) {
         Map<String,Object> map = new HashMap<>();
         map.put("out_code",id);
       aSaleImeiMapper.deleteByMap(map);
        return 0;
    }
}

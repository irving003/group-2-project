package com.javasm.service.ku.impl;

import com.javasm.entity.ku.KManage;
import com.javasm.service.base.base.BaseServiceImpl;
import com.javasm.service.ku.KManageService;
import org.springframework.stereotype.Service;

/**
 * @program: group-2-project
 * @description:
 * @author: lydms
 * @create: 2023-06-11 23:21
 **/
@Service
public class KManageSericeImpl extends BaseServiceImpl<KManage> implements KManageService {
}

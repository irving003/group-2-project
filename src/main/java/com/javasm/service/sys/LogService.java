package com.javasm.service.sys;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.yulichang.toolkit.support.LambdaMeta;
import com.javasm.entity.sys.SLog;
import com.javasm.service.base.BaseService;

import java.util.List;

public interface LogService extends BaseService<SLog> {
    List<SLog> search(LambdaQueryWrapper<SLog> queryWrapper);
}

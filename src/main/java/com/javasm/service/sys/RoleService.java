package com.javasm.service.sys;

import com.javasm.entity.sys.SAuthority;
import com.javasm.entity.sys.SRole;

import java.util.List;

public interface RoleService {
    List<SRole> findAll();

    List<SAuthority> findAuthorityById(Integer id);

    //  条件查询
    List<SRole> findBysRole(SRole sRole);

    // 条件查询
    SAuthority findByAuthority(SAuthority authority);

    //  添加角色
    int add(SRole sRole);

    // 修改角色
    int update(SRole sRole);

    //  删除角色
    int delete(Long roleId);
    int deleteRoleAuthority(Long id);
}

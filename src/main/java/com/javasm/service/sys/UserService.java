package com.javasm.service.sys;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.javasm.entity.sys.SUser;
import com.javasm.mapper.base.MyMapper;
import com.javasm.mapper.sys.UserMapper;
import com.javasm.service.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService extends BaseService<SUser> {

    List<SUser> search(LambdaQueryWrapper<SUser> Wrapper);
}

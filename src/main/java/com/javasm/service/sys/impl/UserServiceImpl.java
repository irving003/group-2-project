package com.javasm.service.sys.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.javasm.entity.sys.SUser;
import com.javasm.mapper.sys.UserMapper;
import com.javasm.service.base.base.BaseServiceImpl;
import com.javasm.service.sys.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl extends BaseServiceImpl<SUser> implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public List<SUser> search(LambdaQueryWrapper<SUser> Wrapper) {
        return userMapper.selectList(Wrapper) ;
    }
}
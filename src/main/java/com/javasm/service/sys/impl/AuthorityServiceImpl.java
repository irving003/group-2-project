package com.javasm.service.sys.impl;

import com.javasm.entity.sys.SAuthority;
import com.javasm.entity.sys.SAuthorityOperation;
import com.javasm.entity.sys.SOperation;
import com.javasm.entity.sys.SUser;
import com.javasm.mapper.sys.AuthorityMapper;
import com.javasm.service.sys.AuthorityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorityServiceImpl implements AuthorityService {
    @Autowired
    private AuthorityMapper authorityMapper;

    @Override
    public List<SAuthority> findAll() {
        return authorityMapper.findAll();
    }

    @Override
    public List<SOperation> findAllOperation(Integer id) {
        return authorityMapper.findAllOperation(id);
    }

    @Override
    public List<SAuthority> findBysAuthority(SAuthority sAuthority) {
        return authorityMapper.findBysAuthority(sAuthority);
    }

    @Override
    public SOperation findByOperation(SOperation operation) {
        return authorityMapper.findByOperation(operation);
    }

    @Override
    public int add(SAuthority sAuthority) {
        int i = 0;
        int i1 = authorityMapper.add(sAuthority);
        if (sAuthority.getOperationN()==null){
            return i1;
        }
        List<String> operationName = sAuthority.getOperationN();
        System.out.println(operationName);
        operationName.forEach(o -> {
            SOperation sOperation = new SOperation();
            sOperation.setOperationName(o);
            SOperation Operation = authorityMapper.findByOperation(sOperation);

            SAuthorityOperation sAuthorityOperation = new SAuthorityOperation();
            sAuthorityOperation.setAuthorityId(sAuthority.getAuthorityId());
            sAuthorityOperation.setOperationId(Operation.getOperationId());
            authorityMapper.addAuthorityOperation(sAuthorityOperation);
        });
        i = 1;
        return i;
    }

    @Override
    public int addAuthorityOperation(SAuthorityOperation authorityOperation) {
        return authorityMapper.addAuthorityOperation(authorityOperation);
    }

    @Override
    public int update(SAuthority sAuthority) {
        int i = 0;
        if (sAuthority.getOperationName() != null) {
//            删除关联表的数据
            authorityMapper.deleteAuthorityOperation(sAuthority.getAuthorityId());
            //        给权限操作内容关联表加数据
            List<String> operationName = sAuthority.getOperationN();

            operationName.forEach(a -> {
                //            通过名称获得编号
                SOperation sOperation = new SOperation();
                sOperation.setOperationName(a);
                SOperation operation= authorityMapper.findByOperation(sOperation);

                SAuthorityOperation sAuthorityOperation = new SAuthorityOperation();
                sAuthorityOperation.setAuthorityId(sAuthority.getAuthorityId());
                sAuthorityOperation.setOperationId(operation.getOperationId());
                authorityMapper.addAuthorityOperation(sAuthorityOperation);
            });
        }
        authorityMapper.update(sAuthority);
        i = 1;
        return i;
    }

    @Override
    public int deleteAuthority(Long authorityId) {
        int i = 0;
        authorityMapper.deleteAuthority(authorityId);
        authorityMapper.deleteAuthorityOperation(authorityId);
        i = 1;
        return i;
    }

    @Override
    public int deleteAuthorityOperation(Long authorityId) {
        return authorityMapper.deleteAuthorityOperation(authorityId);
    }

    @Override
    public List<SUser> findReport(SUser user) {

        return authorityMapper.findReport(user);
    }
}

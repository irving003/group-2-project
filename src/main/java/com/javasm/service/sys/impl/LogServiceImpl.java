package com.javasm.service.sys.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.javasm.entity.sys.SLog;
import com.javasm.mapper.sys.LogMapper;
import com.javasm.service.base.base.BaseServiceImpl;
import com.javasm.service.sys.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LogServiceImpl extends BaseServiceImpl<SLog> implements LogService  {
    @Autowired
    private LogMapper logMapper;
    @Override
    public List<SLog> search(LambdaQueryWrapper<SLog> queryWrapper) {
        return logMapper.selectList(queryWrapper);
    }
}

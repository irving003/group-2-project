package com.javasm.entity.after;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.javasm.entity.base.BaseEntity;
import com.javasm.entity.ye.YGoods;

import java.time.LocalDateTime;
import java.util.List;

public class ASale extends BaseEntity {

  private long imei;
  private String backSet;
  private String backAddress;
  private String backCause;
  private long backMark;
  private String note;
  private String auditIdea;
  private long auditState;
  private String auditer;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private LocalDateTime auditTime;
  private long goodsId;

  private long outCode;
  private long intoCode;


  @TableField(exist = false)
  private List<YGoods> yGoods;
  @TableField(exist = false)
  private List<ASaleImei> aSaleImeis;

  public long getImei() {
    return imei;
  }

  public void setImei(long imei) {
    this.imei = imei;
  }

  public List<ASaleImei> getaSaleImeis() {
    return aSaleImeis;
  }

  public void setaSaleImeis(List<ASaleImei> aSaleImeis) {
    this.aSaleImeis = aSaleImeis;
  }

  public List<YGoods> getyGoods() {
    return yGoods;
  }

  public void setyGoods(List<YGoods> yGoods) {
    this.yGoods = yGoods;
  }

  public String getBackSet() {
    return backSet;
  }

  public void setBackSet(String backSet) {
    this.backSet = backSet;
  }


  public String getBackAddress() {
    return backAddress;
  }

  public void setBackAddress(String backAddress) {
    this.backAddress = backAddress;
  }


  public String getBackCause() {
    return backCause;
  }

  public void setBackCause(String backCause) {
    this.backCause = backCause;
  }


  public long getBackMark() {
    return backMark;
  }

  public void setBackMark(long backMark) {
    this.backMark = backMark;
  }


  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }


  public String getAuditIdea() {
    return auditIdea;
  }

  public void setAuditIdea(String auditIdea) {
    this.auditIdea = auditIdea;
  }


  public long getAuditState() {
    return auditState;
  }

  public void setAuditState(long auditState) {
    this.auditState = auditState;
  }


  public String getAuditer() {
    return auditer;
  }

  public void setAuditer(String auditer) {
    this.auditer = auditer;
  }


  public LocalDateTime getAuditTime() {
    return auditTime;
  }

  public void setAuditTime(LocalDateTime auditTime) {
    this.auditTime = auditTime;
  }


  public long getGoodsId() {
    return goodsId;
  }

  public void setGoodsId(long goodsId) {
    this.goodsId = goodsId;
  }


  public long getOutCode() {
    return outCode;
  }

  public void setOutCode(long outCode) {
    this.outCode = outCode;
  }


  public long getIntoCode() {
    return intoCode;
  }

  public void setIntoCode(long intoCode) {
    this.intoCode = intoCode;
  }



}

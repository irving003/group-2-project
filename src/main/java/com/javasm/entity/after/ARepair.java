package com.javasm.entity.after;


import com.javasm.entity.base.BaseEntity;

import java.io.Serializable;

public class ARepair implements Serializable {


  private String companyName;
  private String companyAddress;
  private long phone;
  private long repairCode;





  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }


  public String getCompanyAddress() {
    return companyAddress;
  }

  public void setCompanyAddress(String companyAddress) {
    this.companyAddress = companyAddress;
  }


  public long getPhone() {
    return phone;
  }

  public void setPhone(long phone) {
    this.phone = phone;
  }


  public long getRepairCode() {
    return repairCode;
  }

  public void setRepairCode(long repairCode) {
    this.repairCode = repairCode;
  }

}

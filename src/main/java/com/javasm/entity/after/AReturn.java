package com.javasm.entity.after;


import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.javasm.entity.B2C.BConsignee;
import com.javasm.entity.B2C.Order;

import com.javasm.entity.base.BaseEntity;
import com.javasm.entity.ye.YGoods;

import java.time.LocalDateTime;
import java.util.List;

public class AReturn extends BaseEntity {

  private long orderId;
  private String complaintName;
  private String complaintSource;
  private String recorder;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private LocalDateTime recordTime;
  private String auditPerson;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private LocalDateTime auditDate;
  private long invoices;
  private long invoicesCode;
  private long returnType;
  private long code;
  private long newCode;
  private String changeName;
  private String changeCause;
  private String repairCode;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private LocalDateTime clientRequestTime;
  private String csReply;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private LocalDateTime csTime;
  private String twoCsReply;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private LocalDateTime twoCsTime;
  private String note;
  private long auditStatus;
  private String faulty;
  private long outCode;
  @TableField(exist = false)
  private List<ASaleImei> aSaleImeis;
  @TableField(exist = false)
  private Order order;
  @TableField(exist = false)
  private BConsignee bConsignee;
  @TableField(exist = false)
  private List<YGoods> yGoods;

  public long getOutCode() {
    return outCode;
  }

  public void setOutCode(long outCode) {
    this.outCode = outCode;
  }

  public List<ASaleImei> getaSaleImeis() {
    return aSaleImeis;
  }

  public void setaSaleImeis(List<ASaleImei> aSaleImeis) {
    this.aSaleImeis = aSaleImeis;
  }

  public Order getOrder() {
    return order;
  }

  public void setOrder(Order order) {
    this.order = order;
  }

  public BConsignee getbConsignee() {
    return bConsignee;
  }

  public void setbConsignee(BConsignee bConsignee) {
    this.bConsignee = bConsignee;
  }

  public List<YGoods> getyGoods() {
    return yGoods;
  }

  public void setyGoods(List<YGoods> yGoods) {
    this.yGoods = yGoods;
  }

  public long getOrderId() {
    return orderId;
  }

  public void setOrderId(long orderId) {
    this.orderId = orderId;
  }


  public String getComplaintName() {
    return complaintName;
  }

  public void setComplaintName(String complaintName) {
    this.complaintName = complaintName;
  }


  public String getComplaintSource() {
    return complaintSource;
  }

  public void setComplaintSource(String complaintSource) {
    this.complaintSource = complaintSource;
  }


  public String getRecorder() {
    return recorder;
  }

  public void setRecorder(String recorder) {
    this.recorder = recorder;
  }


  public LocalDateTime getRecordTime() {
    return recordTime;
  }

  public void setRecordTime(LocalDateTime recordTime) {
    this.recordTime = recordTime;
  }


  public String getAuditPerson() {
    return auditPerson;
  }

  public void setAuditPerson(String auditPerson) {
    this.auditPerson = auditPerson;
  }


  public LocalDateTime getAuditDate() {
    return auditDate;
  }

  public void setAuditDate(LocalDateTime auditDate) {
    this.auditDate = auditDate;
  }


  public long getInvoices() {
    return invoices;
  }

  public void setInvoices(long invoices) {
    this.invoices = invoices;
  }


  public long getInvoicesCode() {
    return invoicesCode;
  }

  public void setInvoicesCode(long invoicesCode) {
    this.invoicesCode = invoicesCode;
  }


  public long getReturnType() {
    return returnType;
  }

  public void setReturnType(long returnType) {
    this.returnType = returnType;
  }


  public long getCode() {
    return code;
  }

  public void setCode(long code) {
    this.code = code;
  }


  public long getNewCode() {
    return newCode;
  }

  public void setNewCode(long newCode) {
    this.newCode = newCode;
  }


  public String getChangeName() {
    return changeName;
  }

  public void setChangeName(String changeName) {
    this.changeName = changeName;
  }


  public String getChangeCause() {
    return changeCause;
  }

  public void setChangeCause(String changeCause) {
    this.changeCause = changeCause;
  }


  public String getRepairCode() {
    return repairCode;
  }

  public void setRepairCode(String repairCode) {
    this.repairCode = repairCode;
  }


  public LocalDateTime getClientRequestTime() {
    return clientRequestTime;
  }

  public void setClientRequestTime(LocalDateTime clientRequestTime) {
    this.clientRequestTime = clientRequestTime;
  }


  public String getCsReply() {
    return csReply;
  }

  public void setCsReply(String csReply) {
    this.csReply = csReply;
  }


  public LocalDateTime getCsTime() {
    return csTime;
  }

  public void setCsTime(LocalDateTime csTime) {
    this.csTime = csTime;
  }


  public String getTwoCsReply() {
    return twoCsReply;
  }

  public void setTwoCsReply(String twoCsReply) {
    this.twoCsReply = twoCsReply;
  }


  public LocalDateTime getTwoCsTime() {
    return twoCsTime;
  }

  public void setTwoCsTime(LocalDateTime twoCsTime) {
    this.twoCsTime = twoCsTime;
  }


  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }



  public long getAuditStatus() {
    return auditStatus;
  }

  public void setAuditStatus(long auditStatus) {
    this.auditStatus = auditStatus;
  }


  public String getFaulty() {
    return faulty;
  }

  public void setFaulty(String faulty) {
    this.faulty = faulty;
  }

}

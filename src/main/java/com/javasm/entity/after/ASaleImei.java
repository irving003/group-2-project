package com.javasm.entity.after;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.javasm.entity.ye.YGoods;

import java.io.Serializable;
import java.util.List;

public class ASaleImei implements Serializable {
  @TableId(type = IdType.AUTO)
  private long id;
  private long outCode;
  private long imei;
  @TableField(exist = false)
  private ASale aSale;
  @TableField(exist = false)
  private List<YGoods> yGoods;

  public ASaleImei(long outCode, long imei) {
    this.outCode = outCode;
    this.imei = imei;
  }

  public ASale getaSale() {
    return aSale;
  }

  public void setaSale(ASale aSale) {
    this.aSale = aSale;
  }

  public List<YGoods> getyGoods() {
    return yGoods;
  }

  public void setyGoods(List<YGoods> yGoods) {
    this.yGoods = yGoods;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public long getOutCode() {
    return outCode;
  }

  public void setOutCode(long outCode) {
    this.outCode = outCode;
  }


  public long getImei() {
    return imei;
  }

  public void setImei(long imei) {
    this.imei = imei;
  }

}

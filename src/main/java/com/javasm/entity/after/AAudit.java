package com.javasm.entity.after;


import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.javasm.entity.base.BaseEntity;

import java.time.LocalDateTime;
//申述管理
public class AAudit extends BaseEntity {

  // 方案名称
  private String schemeName;
  // 方案关键字
  private String schemeKeyet;
  // 方案创建人
  private String schemeBy;
  // 审批人
  private String auditer;
  // 审批时间
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private LocalDateTime auditTime;
  // 审批状态
  private String auditStatus;
  // 方案问题
  private String schemeQuestion;
  // 方案解答
  private String schemeAnswer;
  // 方案场景
  private String schemeScene;
  // 方案备注
  private String schemeRemark;
  // 审批意见
  private String auditIdea;
  // 审核时间
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private LocalDateTime auditDate;


  public String getSchemeName() {
    return schemeName;
  }

  public void setSchemeName(String schemeName) {
    this.schemeName = schemeName;
  }


  public String getSchemeKeyet() {
    return schemeKeyet;
  }

  public void setSchemeKeyet(String schemeKeyet) {
    this.schemeKeyet = schemeKeyet;
  }


  public String getSchemeBy() {
    return schemeBy;
  }

  public void setSchemeBy(String schemeBy) {
    this.schemeBy = schemeBy;
  }



  public String getAuditer() {
    return auditer;
  }

  public void setAuditer(String auditer) {
    this.auditer = auditer;
  }


  public LocalDateTime getAuditTime() {
    return auditTime;
  }

  public void setAuditTime(LocalDateTime auditTime) {
    this.auditTime = auditTime;
  }


  public String getAuditStatus() {
    return auditStatus;
  }

  public void setAuditStatus(String auditStatus) {
    this.auditStatus = auditStatus;
  }


  public String getSchemeQuestion() {
    return schemeQuestion;
  }

  public void setSchemeQuestion(String schemeQuestion) {
    this.schemeQuestion = schemeQuestion;
  }


  public String getSchemeAnswer() {
    return schemeAnswer;
  }

  public void setSchemeAnswer(String schemeAnswer) {
    this.schemeAnswer = schemeAnswer;
  }


  public String getSchemeScene() {
    return schemeScene;
  }

  public void setSchemeScene(String schemeScene) {
    this.schemeScene = schemeScene;
  }


  public String getSchemeRemark() {
    return schemeRemark;
  }

  public void setSchemeRemark(String schemeRemark) {
    this.schemeRemark = schemeRemark;
  }


  public String getAuditIdea() {
    return auditIdea;
  }

  public void setAuditIdea(String auditIdea) {
    this.auditIdea = auditIdea;
  }


  public LocalDateTime getAuditDate() {
    return auditDate;
  }

  public void setAuditDate(LocalDateTime auditDate) {
    this.auditDate = auditDate;
  }

}

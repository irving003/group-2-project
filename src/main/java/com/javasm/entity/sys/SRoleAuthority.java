package com.javasm.entity.sys;


public class SRoleAuthority {

  private long id;
  private long roleId;
  private long authorityId;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public long getRoleId() {
    return roleId;
  }

  public void setRoleId(long roleId) {
    this.roleId = roleId;
  }


  public long getAuthorityId() {
    return authorityId;
  }

  public void setAuthorityId(long authorityId) {
    this.authorityId = authorityId;
  }

  @Override
  public String toString() {
    return "SRoleAuthority{" +
            "id=" + id +
            ", roleId=" + roleId +
            ", authorityId=" + authorityId +
            '}';
  }
}

package com.javasm.entity.sys;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.github.pagehelper.PageInfo;

import java.io.Serializable;
import java.util.List;

public class SUser implements Serializable {
  @TableId(type = IdType.AUTO)
  private long userId;
  private String loginName;
  private String userName;
  private String userDepartment;
  private String userPassword;
  private String userSex;
  private String userStatus;
  private String userJob;
  private String userPhone;
  private String userEmail;
  private String userRemarka;
  private String userRemarkb;
  private String userRemarkc;
  private String userRemarkd;
  private String userRemarke;
  private long userRoleId;
  @TableField(exist = false)
  private PageInfo<SUser> pageInfo;

  public PageInfo<SUser> getPageInfo() {
    return pageInfo;
  }

  public void setPageInfo(PageInfo<SUser> pageInfo) {
    this.pageInfo = pageInfo;
  }
  @TableField(exist = false)
  private String userRoleName;
  @TableField(exist = false)
  private List<SAuthority> authorityName;
  @TableField(exist = false)
  private String authority;
  @TableField(exist = false)
  private List<SOperation> operationName;
  @TableField(exist = false)
  private String operation;

  public String getAuthority() {
    return authority;
  }

  public void setAuthority(String authority) {
    this.authority = authority;
  }

  public String getOperation() {
    return operation;
  }

  public void setOperation(String operation) {
    this.operation = operation;
  }

  public List<SAuthority> getAuthorityName() {
    return authorityName;
  }

  public void setAuthorityName(List<SAuthority> authorityName) {
    this.authorityName = authorityName;
  }

  public List<SOperation> getOperationName() {
    return operationName;
  }

  public void setOperationName(List<SOperation> operationName) {
    this.operationName = operationName;
  }

  public long getUserId() {
    return userId;
  }

  public void setUserId(long userId) {
    this.userId = userId;
  }


  public String getLoginName() {
    return loginName;
  }

  public void setLoginName(String loginName) {
    this.loginName = loginName;
  }


  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }


  public String getUserDepartment() {
    return userDepartment;
  }

  public void setUserDepartment(String userDepartment) {
    this.userDepartment = userDepartment;
  }


  public String getUserPassword() {
    return userPassword;
  }

  public void setUserPassword(String userPassword) {
    this.userPassword = userPassword;
  }


  public String getUserSex() {
    return userSex;
  }

  public void setUserSex(String userSex) {
    this.userSex = userSex;
  }


  public String getUserStatus() {
    return userStatus;
  }

  public void setUserStatus(String userStatus) {
    this.userStatus = userStatus;
  }


  public String getUserJob() {
    return userJob;
  }

  public void setUserJob(String userJob) {
    this.userJob = userJob;
  }


  public String getUserPhone() {
    return userPhone;
  }

  public void setUserPhone(String userPhone) {
    this.userPhone = userPhone;
  }


  public String getUserEmail() {
    return userEmail;
  }

  public void setUserEmail(String userEmail) {
    this.userEmail = userEmail;
  }


  public String getUserRemarka() {
    return userRemarka;
  }

  public void setUserRemarka(String userRemarka) {
    this.userRemarka = userRemarka;
  }


  public String getUserRemarkb() {
    return userRemarkb;
  }

  public void setUserRemarkb(String userRemarkb) {
    this.userRemarkb = userRemarkb;
  }


  public String getUserRemarkc() {
    return userRemarkc;
  }

  public void setUserRemarkc(String userRemarkc) {
    this.userRemarkc = userRemarkc;
  }


  public String getUserRemarkd() {
    return userRemarkd;
  }

  public void setUserRemarkd(String userRemarkd) {
    this.userRemarkd = userRemarkd;
  }


  public String getUserRemarke() {
    return userRemarke;
  }

  public void setUserRemarke(String userRemarke) {
    this.userRemarke = userRemarke;
  }

  public long getUserRoleId() {
    return userRoleId;
  }

  public void setUserRoleId(long userRoleId) {
    this.userRoleId = userRoleId;
  }

  public String getUserRoleName() {
    return userRoleName;
  }

  public void setUserRoleName(String userRoleName) {
    this.userRoleName = userRoleName;
  }

  @Override
  public String toString() {
    return "SUser{" +
            "userId=" + userId +
            ", loginName='" + loginName + '\'' +
            ", userName='" + userName + '\'' +
            ", userDepartment='" + userDepartment + '\'' +
            ", userPassword='" + userPassword + '\'' +
            ", userSex='" + userSex + '\'' +
            ", userStatus='" + userStatus + '\'' +
            ", userJob='" + userJob + '\'' +
            ", userPhone='" + userPhone + '\'' +
            ", userEmail='" + userEmail + '\'' +
            ", userRemarka='" + userRemarka + '\'' +
            ", userRemarkb='" + userRemarkb + '\'' +
            ", userRemarkc='" + userRemarkc + '\'' +
            ", userRemarkd='" + userRemarkd + '\'' +
            ", userRemarke='" + userRemarke + '\'' +
            ", userRoleId=" + userRoleId +
            ", userRoleName='" + userRoleName + '\'' +
            ", authorityName=" + authorityName +
            ", operationName=" + operationName +
            '}';
  }
}

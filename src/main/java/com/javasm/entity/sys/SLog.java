package com.javasm.entity.sys;


import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.github.pagehelper.PageInfo;

import java.io.Serializable;
import java.time.LocalDateTime;

public class SLog implements Serializable {

  private long logId;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
  private LocalDateTime updateTime;
  private String logName;
  private String logModule;
  private String logContent;

  @TableField(exist = false)
  @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
  private LocalDateTime time1;
  @TableField(exist = false)
  @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
  private LocalDateTime time2;
  @TableField(exist = false)
  private PageInfo<SLog> pageInfo;

  @Override
  public String toString() {
    return "SLog{" +
            "logId=" + logId +
            ", updateTime=" + updateTime +
            ", logName='" + logName + '\'' +
            ", logModule='" + logModule + '\'' +
            ", logContent='" + logContent + '\'' +
            ", time1=" + time1 +
            ", time2=" + time2 +
            ", pageInfo=" + pageInfo +
            '}';
  }

  public PageInfo<SLog> getPageInfo() {
    return pageInfo;
  }

  public void setPageInfo(PageInfo<SLog> pageInfo) {
    this.pageInfo = pageInfo;
  }

  public LocalDateTime getTime1() {
    return time1;
  }

  public void setTime1(LocalDateTime time1) {
    this.time1 = time1;
  }

  public LocalDateTime getTime2() {
    return time2;
  }

  public void setTime2(LocalDateTime time2) {
    this.time2 = time2;
  }

  public long getLogId() {
    return logId;
  }

  public void setLogId(long logId) {
    this.logId = logId;
  }


  public LocalDateTime getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(LocalDateTime updateTime) {
    this.updateTime = updateTime;
  }


  public String getLogName() {
    return logName;
  }

  public void setLogName(String logName) {
    this.logName = logName;
  }


  public String getLogModule() {
    return logModule;
  }

  public void setLogModule(String logModule) {
    this.logModule = logModule;
  }


  public String getLogContent() {
    return logContent;
  }

  public void setLogContent(String logContent) {
    this.logContent = logContent;
  }

}

package com.javasm.entity.sys;


public class SAuthorityOperation {

  private long id;
  private long authorityId;
  private long operationId;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public long getAuthorityId() {
    return authorityId;
  }

  public void setAuthorityId(long authorityId) {
    this.authorityId = authorityId;
  }


  public long getOperationId() {
    return operationId;
  }

  public void setOperationId(long operationId) {
    this.operationId = operationId;
  }

}

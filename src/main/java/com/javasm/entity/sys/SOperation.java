package com.javasm.entity.sys;


public class SOperation {

  private long operationId;
  private String operationName;
  private String menuUrl;

  public String getMenuUrl() {
    return menuUrl;
  }

  public void setMenuUrl(String menuUrl) {
    this.menuUrl = menuUrl;
  }

  public long getOperationId() {
    return operationId;
  }

  public void setOperationId(long operationId) {
    this.operationId = operationId;
  }


  public String getOperationName() {
    return operationName;
  }

  public void setOperationName(String operationName) {
    this.operationName = operationName;
  }

}

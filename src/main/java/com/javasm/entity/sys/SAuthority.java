package com.javasm.entity.sys;


import com.github.pagehelper.PageInfo;

import java.util.List;

public class SAuthority {

    private long authorityId;
    private String authorityName;
    private String authorityDescription;

    private List<SOperation> operationName;
    private String operation;
    private List<String> operationN;

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public List<String> getOperationN() {
        return operationN;
    }

    public void setOperationN(List<String> operationN) {
        this.operationN = operationN;
    }

    private PageInfo<SAuthority> pageInfo;

    public PageInfo<SAuthority> getPageInfo() {
        return pageInfo;
    }

    public void setPageInfo(PageInfo<SAuthority> pageInfo) {
        this.pageInfo = pageInfo;
    }

    public List<SOperation> getOperationName() {
        return operationName;
    }

    public void setOperationName(List<SOperation> operationName) {
        this.operationName = operationName;
    }

    public long getAuthorityId() {
        return authorityId;
    }

    public void setAuthorityId(long authorityId) {
        this.authorityId = authorityId;
    }

    @Override
    public String toString() {
        return "SAuthority{" +
                "authorityId=" + authorityId +
                ", authorityName='" + authorityName + '\'' +
                ", authorityDescription='" + authorityDescription + '\'' +
                ", operationName=" + operationName +
                ", operation='" + operation + '\'' +
                ", operationN=" + operationN +
                ", pageInfo=" + pageInfo +
                '}';
    }

    public String getAuthorityName() {
        return authorityName;
    }

    public void setAuthorityName(String authorityName) {
        this.authorityName = authorityName;
    }


    public String getAuthorityDescription() {
        return authorityDescription;
    }

    public void setAuthorityDescription(String authorityDescription) {
        this.authorityDescription = authorityDescription;
    }

}

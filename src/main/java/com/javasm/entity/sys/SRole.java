package com.javasm.entity.sys;


import com.github.pagehelper.PageInfo;

import java.io.Serializable;
import java.util.List;

public class SRole implements Serializable {

  private long roleId;
  private String roleNumber;
  private String roleName;
  private String roleDescription;
  private String roleStatus;
  private String roleDepartmenrt;

  private List<SAuthority> authorityName;
  private String authority;
  private PageInfo<SRole> pageInfo;
  private List<String> authorityN;

  public List<String> getAuthorityN() {
    return authorityN;
  }

  public void setAuthorityN(List<String> authorityN) {
    this.authorityN = authorityN;
  }

  public String getAuthority() {
    return authority;
  }

  public void setAuthority(String authority) {
    this.authority = authority;
  }

  public PageInfo<SRole> getPageInfo() {
    return pageInfo;
  }

  public void setPageInfo(PageInfo<SRole> pageInfo) {
    this.pageInfo = pageInfo;
  }

  public List<SAuthority> getAuthorityName() {
    return authorityName;
  }

  public void setAuthorityName(List<SAuthority> authorityName) {
    this.authorityName = authorityName;
  }

  public long getRoleId() {
    return roleId;
  }

  public void setRoleId(long roleId) {
    this.roleId = roleId;
  }

  public String getRoleNumber() {
    return roleNumber;
  }

  public void setRoleNumber(String roleNumber) {
    this.roleNumber = roleNumber;
  }


  public String getRoleName() {
    return roleName;
  }

  public void setRoleName(String roleName) {
    this.roleName = roleName;
  }


  public String getRoleDescription() {
    return roleDescription;
  }

  public void setRoleDescription(String roleDescription) {
    this.roleDescription = roleDescription;
  }


  public String getRoleStatus() {
    return roleStatus;
  }

  public void setRoleStatus(String roleStatus) {
    this.roleStatus = roleStatus;
  }


  public String getRoleDepartmenrt() {
    return roleDepartmenrt;
  }

  public void setRoleDepartmenrt(String roleDepartmenrt) {
    this.roleDepartmenrt = roleDepartmenrt;
  }

  @Override
  public String toString() {
    return "SRole{" +
            "roleId=" + roleId +
            ", roleNumber='" + roleNumber + '\'' +
            ", roleName='" + roleName + '\'' +
            ", roleDescription='" + roleDescription + '\'' +
            ", roleStatus='" + roleStatus + '\'' +
            ", roleDepartmenrt='" + roleDepartmenrt + '\'' +
            ", authorityName=" + authorityName +
            '}';
  }
}

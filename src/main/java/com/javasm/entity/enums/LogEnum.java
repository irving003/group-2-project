package com.javasm.entity.enums;

public enum  LogEnum {
    /**
     * 操作类型
     */
    INSERT,//新增
    DELETE,//删除
    UPDATE,//更新
    EXPORT,//导出
    SELECT,//查询
    LOGON,//登录
    OTHER;//其他
}

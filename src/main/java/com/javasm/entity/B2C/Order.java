package com.javasm.entity.B2C;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.javasm.entity.base.BaseEntity;
import com.javasm.entity.ye.YGoods;


import java.time.LocalDateTime;
import java.util.List;
import java.util.regex.Pattern;

@TableName("b_order")
public class Order extends BaseEntity {

  private Long orderId;
  private String orderType;
  private String orderAction;
  private String busType;
  private String payType;
  private String shipMode;
  private String orderState;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private LocalDateTime orderTime;
  private String goodDesc;
  private String couNum;
  private String outCreator;


  @TableField(exist = false)
  private BConsignee bConsignee;
  @TableField(exist = false)
  private List<YGoods> goods;


  public String getOutCreator() {
    return outCreator;
  }

  public void setOutCreator(String outCreator) {
    this.outCreator = outCreator;
  }

  public String getCouNum() {
    return couNum;
  }

  public void setCouNum(String couNum) {
    this.couNum = couNum;
  }

  public BConsignee getbConsignee() {
    return bConsignee;
  }

  public void setbConsignee(BConsignee bConsignee) {
    this.bConsignee = bConsignee;
  }

  public List<YGoods> getGoods() {
    return goods;
  }

  public void setGoods(List<YGoods> goods) {
    this.goods = goods;
  }

  public Long getOrderId() {
    return orderId;
  }

  public void setOrderId(Long orderId) {
    this.orderId = orderId;
  }

  public String getOrderType() {
    return orderType;
  }

  public void setOrderType(String orderType) {
    this.orderType = orderType;
  }


  public String getOrderAction() {
    return orderAction;
  }

  public void setOrderAction(String orderAction) {
    this.orderAction = orderAction;
  }


  public String getBusType() {
    return busType;
  }

  public void setBusType(String busType) {
    this.busType = busType;
  }


  public String getPayType() {
    return payType;
  }

  public void setPayType(String payType) {
    this.payType = payType;
  }


  public String getShipMode() {
    return shipMode;
  }

  public void setShipMode(String shipMode) {
    this.shipMode = shipMode;
  }


  public String getOrderState() {
    return orderState;
  }

  public void setOrderState(String orderState) {
    this.orderState = orderState;
  }


  public LocalDateTime getOrderTime() {
    return orderTime;
  }

  public void setOrderTime(LocalDateTime orderTime) {
    this.orderTime = orderTime;
  }


  public String getGoodDesc() {
    return goodDesc;
  }

  public void setGoodDesc(String goodDesc) {
    this.goodDesc = goodDesc;
  }



}

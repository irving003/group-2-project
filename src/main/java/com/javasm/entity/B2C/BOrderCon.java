package com.javasm.entity.B2C;


public class BOrderCon {

  private long id;
  private long orderId;
  private long congsigneeId;
  private long goodsId;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public long getOrderId() {
    return orderId;
  }

  public void setOrderId(long orderId) {
    this.orderId = orderId;
  }


  public long getCongsigneeId() {
    return congsigneeId;
  }

  public void setCongsigneeId(long congsigneeId) {
    this.congsigneeId = congsigneeId;
  }


  public long getGoodsId() {
    return goodsId;
  }

  public void setGoodsId(long goodsId) {
    this.goodsId = goodsId;
  }

}

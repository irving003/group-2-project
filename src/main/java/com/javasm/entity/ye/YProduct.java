package com.javasm.entity.ye;


public class YProduct {

  private long id;
  private long coding;
  private String classify;
  private String brand;
  private String type;
  private String color;
  private String businessPattern;
  private String purchasing_Pattern;
  private String distribution;
  private String standby;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getCoding() {
    return coding;
  }

  public void setCoding(long coding) {
    this.coding = coding;
  }

  public String getClassify() {
    return classify;
  }

  public void setClassify(String classify) {
    this.classify = classify;
  }


  public String getBrand() {
    return brand;
  }

  public void setBrand(String brand) {
    this.brand = brand;
  }


  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }


  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  public String getBusinessPattern() {
    return businessPattern;
  }

  public void setBusinessPattern(String businessPattern) {
    this.businessPattern = businessPattern;
  }

  public String getPurchasing_Pattern() {
    return purchasing_Pattern;
  }

  public void setPurchasing_Pattern(String purchasing_Pattern) {
    this.purchasing_Pattern = purchasing_Pattern;
  }

  public String getDistribution() {
    return distribution;
  }

  public void setDistribution(String distribution) {
    this.distribution = distribution;
  }

  public String getStandby() {
    return standby;
  }

  public void setStandby(String standby) {
    this.standby = standby;
  }

  @Override
  public String toString() {
    return "YProduct{" +
            "id=" + id +
            ", coding='" + coding + '\'' +
            ", classify='" + classify + '\'' +
            ", brand='" + brand + '\'' +
            ", type='" + type + '\'' +
            ", color='" + color + '\'' +
            ", businessPattern='" + businessPattern + '\'' +
            ", purchasing_Pattern='" + purchasing_Pattern + '\'' +
            ", distribution='" + distribution + '\'' +
            ", standby='" + standby + '\'' +
            '}';
  }
}

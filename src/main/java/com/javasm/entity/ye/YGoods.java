package com.javasm.entity.ye;


import lombok.Data;

import java.time.LocalDateTime;
@Data
public class YGoods {

  private long id;
  private String goodsName;
  private String goodsClassify;
  private String goodsBrand;
  private String goodsModel;
  private String goodsColor;
  private long goodsPrice;
  private long goodsNumber;
  private String goodsType;
  private String goodsState;
  private long goodsGrade;
  private long purchasePrice;
  private long marketPrice;
  private long shopPrice;
  private long promotionPrice;
  private long inventory;
  private String purchase;
  private long imei;
  private long productCode;
  private long procurementCost;
  private String purchasingDepartment;
  private String auditOpinion;
  private String auditor;
  private LocalDateTime auditTime;
  private String commoditySupplier;
  private String trackingInformation;
  private String retrofit;

}

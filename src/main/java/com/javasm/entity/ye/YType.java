package com.javasm.entity.ye;


public class YType {

  private long id;
  private String goodsName;
  private String brandDescribe;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getGoodsName() {
    return goodsName;
  }

  public void setGoodsName(String goodsName) {
    this.goodsName = goodsName;
  }

  public String getBrandDescribe() {
    return brandDescribe;
  }

  public void setBrandDescribe(String brandDescribe) {
    this.brandDescribe = brandDescribe;
  }

  @Override
  public String toString() {
    return "YType{" +
            "id=" + id +
            ", goodsName='" + goodsName + '\'' +
            ", brandDescribe='" + brandDescribe + '\'' +
            '}';
  }
}

package com.javasm.entity.ye;


import lombok.Data;

import java.time.LocalDateTime;
//活动实体类
@Data
public class YActivity {

  private long id;
  private String name;
  private LocalDateTime beginTime;
  private long originalPrice;
  private long discountPrice;
  private LocalDateTime effectiveTime;
  private LocalDateTime failure_Time;
  private String state;
  private String mode;
  private String proposer;
  private LocalDateTime data;
  private String progressOfActivity;
  private String remark;
  private String event_Planning;
  private String usageMode;
  private String itemsOfApplication;


}

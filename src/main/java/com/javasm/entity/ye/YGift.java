package com.javasm.entity.ye;


import java.time.LocalDateTime;

public class YGift {

  private long id;
  private String name;
  private String type;
  private String model;
  private long inventory;
  private long price;
  private long cost;
  private String department;
  private String state;
  private String brand;
  private long number;
  private String color;
  private String auditStatus;
  private String auditOpinion;
  private String auditor;
  private LocalDateTime auditTime;
  private LocalDateTime creationTime;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }


  public long getInventory() {
    return inventory;
  }

  public void setInventory(long inventory) {
    this.inventory = inventory;
  }


  public long getPrice() {
    return price;
  }

  public void setPrice(long price) {
    this.price = price;
  }


  public long getCost() {
    return cost;
  }

  public void setCost(long cost) {
    this.cost = cost;
  }


  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }


  public String getBrand() {
    return brand;
  }

  public void setBrand(String brand) {
    this.brand = brand;
  }


  public long getNumber() {
    return number;
  }

  public void setNumber(long number) {
    this.number = number;
  }


  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public String getDepartment() {
    return department;
  }

  public void setDepartment(String department) {
    this.department = department;
  }

  public String getAuditStatus() {
    return auditStatus;
  }

  public void setAuditStatus(String auditStatus) {
    this.auditStatus = auditStatus;
  }

  public String getAuditOpinion() {
    return auditOpinion;
  }

  public void setAuditOpinion(String auditOpinion) {
    this.auditOpinion = auditOpinion;
  }

  public String getAuditor() {
    return auditor;
  }

  public void setAuditor(String auditor) {
    this.auditor = auditor;
  }

  public LocalDateTime getAuditTime() {
    return auditTime;
  }

  public void setAuditTime(LocalDateTime auditTime) {
    this.auditTime = auditTime;
  }

  public LocalDateTime getCreationTime() {
    return creationTime;
  }

  public void setCreationTime(LocalDateTime creationTime) {
    this.creationTime = creationTime;
  }

  @Override
  public String toString() {
    return "YGift{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", type='" + type + '\'' +
            ", model='" + model + '\'' +
            ", inventory=" + inventory +
            ", price=" + price +
            ", cost=" + cost +
            ", department='" + department + '\'' +
            ", state='" + state + '\'' +
            ", brand='" + brand + '\'' +
            ", number=" + number +
            ", color='" + color + '\'' +
            ", auditStatus='" + auditStatus + '\'' +
            ", auditOpinion='" + auditOpinion + '\'' +
            ", auditor='" + auditor + '\'' +
            ", auditTime=" + auditTime +
            ", creationTime=" + creationTime +
            '}';
  }
}

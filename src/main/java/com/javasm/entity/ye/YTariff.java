package com.javasm.entity.ye;


import java.time.LocalDateTime;

public class YTariff {

  private long id;
  private long goodsId;
  private String goodsName;
  //原商城价
  private long originalPrice;
  //调整后的商城价
  private long discountPrice;
  //市场价
  private long marketPrice;
  //调整后的市场价
  private long storePrice;
  private LocalDateTime effectiveTime;
  private LocalDateTime failureTime;
  private String cause;
  private String remark;
  private String auditor;
  private LocalDateTime auditTime;
  private String auditStatus;
  private String auditRemark;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getGoodsId() {
    return goodsId;
  }

  public void setGoodsId(long goodsId) {
    this.goodsId = goodsId;
  }

  public String getGoodsName() {
    return goodsName;
  }

  public void setGoodsName(String goodsName) {
    this.goodsName = goodsName;
  }


  public long getOriginalPrice() {
    return originalPrice;
  }

  public void setOriginalPrice(long originalPrice) {
    this.originalPrice = originalPrice;
  }

  public long getDiscountPrice() {
    return discountPrice;
  }

  public void setDiscountPrice(long discountPrice) {
    this.discountPrice = discountPrice;
  }

  public LocalDateTime getEffectiveTime() {
    return effectiveTime;
  }

  public void setEffectiveTime(LocalDateTime effectiveTime) {
    this.effectiveTime = effectiveTime;
  }

  public LocalDateTime getFailureTime() {
    return failureTime;
  }

  public void setFailureTime(LocalDateTime failureTime) {
    this.failureTime = failureTime;
  }


  public long getMarketPrice() {
    return marketPrice;
  }

  public void setMarketPrice(long marketPrice) {
    this.marketPrice = marketPrice;
  }

  public long getStorePrice() {
    return storePrice;
  }

  public void setStorePrice(long storePrice) {
    this.storePrice = storePrice;
  }

  public String getCause() {
    return cause;
  }

  public void setCause(String cause) {
    this.cause = cause;
  }

  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }

  public String getAuditor() {
    return auditor;
  }

  public void setAuditor(String auditor) {
    this.auditor = auditor;
  }

  public LocalDateTime getAuditTime() {
    return auditTime;
  }

  public void setAuditTime(LocalDateTime auditTime) {
    this.auditTime = auditTime;
  }

  public String getAuditStatus() {
    return auditStatus;
  }

  public void setAuditStatus(String auditStatus) {
    this.auditStatus = auditStatus;
  }

  public String getAuditRemark() {
    return auditRemark;
  }

  public void setAuditRemark(String auditRemark) {
    this.auditRemark = auditRemark;
  }

  @Override
  public String toString() {
    return "YTariff{" +
            "id=" + id +
            ", goodsId=" + goodsId +
            ", goodsName='" + goodsName + '\'' +
            ", originalPrice=" + originalPrice +
            ", discountPrice=" + discountPrice +
            ", marketPrice=" + marketPrice +
            ", storePrice=" + storePrice +
            ", effectiveTime=" + effectiveTime +
            ", failureTime=" + failureTime +
            ", cause='" + cause + '\'' +
            ", remark='" + remark + '\'' +
            ", auditor='" + auditor + '\'' +
            ", auditTime=" + auditTime +
            ", auditStatus='" + auditStatus + '\'' +
            ", auditRemark='" + auditRemark + '\'' +
            '}';
  }
}

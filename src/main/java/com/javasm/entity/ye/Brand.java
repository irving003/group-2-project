package com.javasm.entity.ye;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.javasm.entity.base.BaseEntity;

import java.time.LocalDateTime;

/**
 * @author: Author
 * @className: Brand
 * @description:
 * @date: 2023/6/5 11:23
 * @since: 11
 */
@TableName("y_brand")
public class Brand extends BaseEntity {


    /**
     * 品牌名称
     */
    private String brandName;

    /**
     * 品牌站点
     */
    private String brandSite;

    /**
     * 品牌描述
     */
    private String brandDesc;

    /**
     * 品牌logo
     */
    private String brandLogo;





    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getBrandSite() {
        return brandSite;
    }

    public void setBrandSite(String brandSite) {
        this.brandSite = brandSite;
    }

    public String getBrandDesc() {
        return brandDesc;
    }

    public void setBrandDesc(String brandDesc) {
        this.brandDesc = brandDesc;
    }

    public String getBrandLogo() {
        return brandLogo;
    }

    public void setBrandLogo(String brandLogo) {
        this.brandLogo = brandLogo;
    }

}

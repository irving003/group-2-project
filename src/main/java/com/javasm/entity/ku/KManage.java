package com.javasm.entity.ku;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.time.LocalDateTime;
// 库存管理
@TableName("k_manage")
public class KManage {

  @TableId(type = IdType.AUTO)
  private long id;
  private String shoppingId;
  private String shoppingType;
  private String suppliersName;
  private String suppliersId;
  private String shoppingStatus;
  private LocalDateTime shoppingDate;
  private String invoicesType;
  private LocalDateTime invoicesDate;
  private String goodsName;
  private String goodsId;
  private String auditStatus;
  private double goodsPrice;
  private long goodsNum;
  private String noTaxprice;
  private String taxprice;
  private String taxpriceSum;
  private String createInvoicer;
  private String auditer;
  private String goodsSid;
  private String goodsColor;
  private long shoppingNum;
  private String giftsId;
  private String name;
  private long number;
  private long giftsInto;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public String getShoppingId() {
    return shoppingId;
  }

  public void setShoppingId(String shoppingId) {
    this.shoppingId = shoppingId;
  }


  public String getShoppingType() {
    return shoppingType;
  }

  public void setShoppingType(String shoppingType) {
    this.shoppingType = shoppingType;
  }


  public String getSuppliersName() {
    return suppliersName;
  }

  public void setSuppliersName(String suppliersName) {
    this.suppliersName = suppliersName;
  }


  public String getSuppliersId() {
    return suppliersId;
  }

  public void setSuppliersId(String suppliersId) {
    this.suppliersId = suppliersId;
  }


  public String getShoppingStatus() {
    return shoppingStatus;
  }

  public void setShoppingStatus(String shoppingStatus) {
    this.shoppingStatus = shoppingStatus;
  }


  public LocalDateTime getShoppingDate() {
    return shoppingDate;
  }

  public void setShoppingDate(LocalDateTime shoppingDate) {
    this.shoppingDate = shoppingDate;
  }


  public String getInvoicesType() {
    return invoicesType;
  }

  public void setInvoicesType(String invoicesType) {
    this.invoicesType = invoicesType;
  }


  public LocalDateTime getInvoicesDate() {
    return invoicesDate;
  }

  public void setInvoicesDate(LocalDateTime invoicesDate) {
    this.invoicesDate = invoicesDate;
  }


  public String getGoodsName() {
    return goodsName;
  }

  public void setGoodsName(String goodsName) {
    this.goodsName = goodsName;
  }


  public String getGoodsId() {
    return goodsId;
  }

  public void setGoodsId(String goodsId) {
    this.goodsId = goodsId;
  }


  public String getAuditStatus() {
    return auditStatus;
  }

  public void setAuditStatus(String auditStatus) {
    this.auditStatus = auditStatus;
  }


  public double getGoodsPrice() {
    return goodsPrice;
  }

  public void setGoodsPrice(double goodsPrice) {
    this.goodsPrice = goodsPrice;
  }


  public long getGoodsNum() {
    return goodsNum;
  }

  public void setGoodsNum(long goodsNum) {
    this.goodsNum = goodsNum;
  }


  public String getNoTaxprice() {
    return noTaxprice;
  }

  public void setNoTaxprice(String noTaxprice) {
    this.noTaxprice = noTaxprice;
  }


  public String getTaxprice() {
    return taxprice;
  }

  public void setTaxprice(String taxprice) {
    this.taxprice = taxprice;
  }


  public String getTaxpriceSum() {
    return taxpriceSum;
  }

  public void setTaxpriceSum(String taxpriceSum) {
    this.taxpriceSum = taxpriceSum;
  }


  public String getCreateInvoicer() {
    return createInvoicer;
  }

  public void setCreateInvoicer(String createInvoicer) {
    this.createInvoicer = createInvoicer;
  }


  public String getAuditer() {
    return auditer;
  }

  public void setAuditer(String auditer) {
    this.auditer = auditer;
  }


  public String getGoodsSid() {
    return goodsSid;
  }

  public void setGoodsSid(String goodsSid) {
    this.goodsSid = goodsSid;
  }


  public String getGoodsColor() {
    return goodsColor;
  }

  public void setGoodsColor(String goodsColor) {
    this.goodsColor = goodsColor;
  }


  public long getShoppingNum() {
    return shoppingNum;
  }

  public void setShoppingNum(long shoppingNum) {
    this.shoppingNum = shoppingNum;
  }


  public String getGiftsId() {
    return giftsId;
  }

  public void setGiftsId(String giftsId) {
    this.giftsId = giftsId;
  }


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  public long getNumber() {
    return number;
  }

  public void setNumber(long number) {
    this.number = number;
  }


  public long getGiftsInto() {
    return giftsInto;
  }

  public void setGiftsInto(long giftsInto) {
    this.giftsInto = giftsInto;
  }

}

package com.javasm;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author: Author
 * @className: MainApplication
 * @description:
 * @date: 2023/6/5 11:19
 * @since: 11 
 */
@SpringBootApplication
// 扫描映射文件和接口
@MapperScan("com.javasm.mapper")
// 开启事务管理
@EnableTransactionManagement
public class MainApplication {
    public static void main(String[] args) {
        SpringApplication.run(MainApplication.class,args);
    }
}

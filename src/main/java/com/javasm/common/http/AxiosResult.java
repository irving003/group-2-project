package com.javasm.common.http;

import com.fasterxml.jackson.annotation.JsonInclude;




/**
 * @author: Author
 * @className: AxiosResult
 * @description:
 * @date: 2023/6/5 14:33
 * @since: 11
 */
// 不是null的数据才会携带到返回数据中
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AxiosResult<T> {
    // 响应状态
    private int status;

    // 响应消息
    private String message;

    // 响应数据
    private T data;



    private AxiosResult(AxiosStatus axiosStatus,T t){
        this.status = axiosStatus.getStatus();
        this.message = axiosStatus.getMessage();
        this.data = t;
    }


    private static <T>  AxiosResult<T> getInstance(AxiosStatus axiosStatus,T t){
        return new AxiosResult<T>(axiosStatus,t);
    }

    // 成功方法
    public static <T> AxiosResult<T> success(){
        return getInstance(AxiosStatus.OK,null);
    }

    // 成功并携带数据
    public static <T> AxiosResult<T> success(T t){
        return getInstance(AxiosStatus.OK,t);
    }

    // 错误方法
    public static <T> AxiosResult<T> error(){
        return getInstance(AxiosStatus.ERROR,null);
    }

    // 错误 自定义状态码
    public static <T> AxiosResult<T> error(AxiosStatus axiosStatus,T t){
        return getInstance(axiosStatus,t);
    }



    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}

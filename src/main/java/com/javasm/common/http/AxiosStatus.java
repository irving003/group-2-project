package com.javasm.common.http;

/**
 * @author: Author
 * @className: AxiosStatus
 * @description:
 * @date: 2023/6/5 14:34
 * @since: 11
 */
public enum AxiosStatus {
    OK(20000,"操作成功"),
    ERROR(50000,"操作失败");

    private int status;

    private String message;

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    AxiosStatus(int status, String message) {
        this.status = status;
        this.message = message;
    }
}

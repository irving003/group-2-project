package com.javasm.common.tools;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;


/**
 * 导出报表工具类
 */
public class ExportExcelUtil {
    /**
     *
     * @param title 标题
     * @param headers  表头
     * @param values  表中元素
     * @return
     */
    public static HSSFWorkbook getHSSFWorkbook(String title, String[] titles, String headers[], String [] values,String[][] values1){

        //创建一个HSSFWorkbook，对应一个Excel文件
        HSSFWorkbook hssfWorkbook = new HSSFWorkbook();

        //在workbook中添加一个sheet,对应Excel文件中的sheet
        HSSFSheet hssfSheet = hssfWorkbook.createSheet(title);

        //创建标题合并行
        hssfSheet.addMergedRegion(new CellRangeAddress(0,(short)0,0,(short)5));
        //合并次级标题行
        for (int i = 1; i <=3; i++) {
            hssfSheet.addMergedRegion(new CellRangeAddress(i,(short)i,1,(short)2));
        }
        for (int i = 1; i <=3; i++) {
            hssfSheet.addMergedRegion(new CellRangeAddress(i,(short)i,4,(short)5));
        }
        hssfSheet.addMergedRegion(new CellRangeAddress(4,(short)4,0,(short)5));
        for (int i = 5; i <=10; i++) {
            hssfSheet.addMergedRegion(new CellRangeAddress(i,(short)i,1,(short)2));
        }
        for (int i = 5; i <=10; i++) {
            hssfSheet.addMergedRegion(new CellRangeAddress(i,(short)i,4,(short)5));
        }
        hssfSheet.addMergedRegion(new CellRangeAddress(11,(short)11,1,(short)5));
        hssfSheet.addMergedRegion(new CellRangeAddress(12,(short)13,0,(short)0));
        hssfSheet.addMergedRegion(new CellRangeAddress(12,(short)13,1,(short)5));
        hssfSheet.addMergedRegion(new CellRangeAddress(14,(short)14,0,(short)4));


        //设置标题样式
        HSSFCellStyle style = hssfWorkbook.createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER);   //设置居中样式
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        //设置标题字体
        Font titleFont = hssfWorkbook.createFont();
        titleFont.setFontHeightInPoints((short) 14);
        style.setFont(titleFont);

        //设置值表头样式 设置表头居中
        HSSFCellStyle hssfCellStyle = hssfWorkbook.createCellStyle();
        hssfCellStyle.setAlignment(HorizontalAlignment.CENTER);   //设置居中样式
        hssfCellStyle.setBorderBottom(BorderStyle.THIN);
        hssfCellStyle.setBorderLeft(BorderStyle.THIN);
        hssfCellStyle.setBorderRight(BorderStyle.THIN);
        hssfCellStyle.setBorderTop(BorderStyle.THIN);

        //设置表内容样式
        //创建单元格，并设置值表头 设置表头居中
        HSSFCellStyle style1 = hssfWorkbook.createCellStyle();
        style1.setBorderBottom(BorderStyle.THIN);
        style1.setBorderLeft(BorderStyle.THIN);
        style1.setBorderRight(BorderStyle.THIN);
        style1.setBorderTop(BorderStyle.THIN);

        //产生标题行
        //---订单基本资料----
        HSSFRow hssfRow = hssfSheet.createRow(0);
        HSSFCell cell = hssfRow.createCell(0);
        cell.setCellValue(titles[0]);
        cell.setCellStyle(style);


        //产生表头
        int index=0;
        int valIndex=0;
        for (int i = 1; i <= 3; i++) {
            HSSFRow row1 = hssfSheet.createRow(i);
            for (int j = 0; j < 6; j++) {
                HSSFCell hssfCell = row1.createCell(j);
                if (j == 0 || j == 3) {
                    hssfCell.setCellValue(headers[index++]);
                    hssfCell.setCellStyle(hssfCellStyle);
                } else {
                    if (j == 1 || j == 4) {
                        hssfCell.setCellValue(values[valIndex++]);
                        hssfCell.setCellStyle(style1);
                    }
                    hssfCell.setCellStyle(style1);
                }
            }
        }
        //收货人资料------
        HSSFRow row4 = hssfSheet.createRow(4);
        HSSFCell cell41 = row4.createCell(0);
        cell41.setCellValue(titles[1]);
        cell41.setCellStyle(style);

        for (int i = 5; i <=10 ; i++) {
            HSSFRow row5 = hssfSheet.createRow(i);
            for (int j = 0; j < 6; j++) {
                HSSFCell hssfCell = row5.createCell(j);
                if (j==0||j==3){
                    hssfCell.setCellValue(headers[index++]);
                    hssfCell.setCellStyle(hssfCellStyle);
                }else {
                    if (j==1||j==4){
                        hssfCell.setCellValue(values[valIndex++]);
                        hssfCell.setCellStyle(style1);
                    }
                    hssfCell.setCellStyle(style1);
                }
            }
        }
        HSSFRow row11 = hssfSheet.createRow(11);
        HSSFCell cell110 = row11.createCell(0);
        cell110.setCellValue(headers[index++]);
        cell110.setCellStyle(hssfCellStyle);

        HSSFCell cell111 = row11.createCell(1);
        cell111.setCellValue(values[valIndex++]);
        cell111.setCellStyle(style1);
        for (int i = 2; i <= 5; i++) {
            HSSFCell cell11 = row11.createCell(i);
            cell11.setCellStyle(style1);
        }

        HSSFRow row12 = hssfSheet.createRow(12);
        HSSFCell cell120 = row12.createCell(0);
        cell120.setCellValue(headers[index++]);
        cell120.setCellStyle(style1);
        for (int i = 1; i <= 5; i++) {
            HSSFCell cell12 = row12.createCell(i);
            cell12.setCellStyle(style1);
        }
        HSSFRow row13 = hssfSheet.createRow(13);
        for (int i = 0; i <= 5; i++) {
            HSSFCell cell13 = row13.createCell(i);
            cell13.setCellStyle(style1);
        }

        HSSFCell cell121 = row12.createCell(1);
        cell121.setCellValue(values[valIndex++]);
        cell121.setCellStyle(style1);

        HSSFRow row14 = hssfSheet.createRow(14);
        HSSFCell cell140 = row14.createCell(0);
        cell140.setCellValue(titles[2]);
        cell140.setCellStyle(style);

        HSSFRow row15 = hssfSheet.createRow(15);

        for (int i = 0; i < 5; i++) {
            HSSFCell hssfCell = row15.createCell(i);
            hssfCell.setCellValue(headers[index++]);
            hssfCell.setCellStyle(hssfCellStyle);
        }

        //创建内容
        for (int i = 0; i <values1.length; i++){
            HSSFRow row = hssfSheet.createRow(i+16);
            for (int j = 0; j < values1[i].length; j++){
                //将内容按顺序赋给对应列对象
                HSSFCell hssfCell = row.createCell(j);
                hssfCell.setCellValue(values1[i][j]);
                hssfCell.setCellStyle(style1);
            }
        }


        return hssfWorkbook;
    }
}

package com.javasm.common.config;

import com.javasm.controller.aspect.AspectController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
    @Autowired
    private AspectController aspectController;
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(aspectController)
                .addPathPatterns("/**")
                // 那些路径不拦截
                .excludePathPatterns("/login/verify", "/tu");
    }
}



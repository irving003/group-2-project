package com.javasm.common.page;


import java.util.List;

/**
 * @author: Author
 * @className: PageBean
 * @description:
 * @date: 2023/6/5 14:40
 * @since: 11
 */
public class PageBean<T>{

    private Long total;

    private List<T> data;

    private PageBean(Long total,List<T> data){
        this.total = total;
        this.data = data;
    }

    public Long getTotal() {
        return total;
    }

    public List<T> getData() {
        return data;
    }

    public static <T> PageBean<T> initData(Long total, List<T> data){
        return new PageBean<T>(total,data);
    }
}

package com.javasm.common.page;

import com.github.pagehelper.PageInfo;
import com.javasm.common.http.AxiosResult;
import com.javasm.entity.B2C.Order;

import java.util.List;

/**
 * 获取分页查询数据
 * @param <T>
 */

public class PageQuery<T> {

    public static <T> AxiosResult<PageBean<T>> search(List<T> search){
        PageInfo<T> pageInfo = new PageInfo<>(search);
        long total = pageInfo.getTotal();
        PageBean<T> pageBean = PageBean.initData(total, search);
        return AxiosResult.success(pageBean);
    }
}

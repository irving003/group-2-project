package com.javasm.controller.ye;

import com.javasm.common.http.AxiosResult;
import com.javasm.controller.base.BaseController;
import com.javasm.entity.ye.Brand;
import com.javasm.service.ye.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author: Author
 * @className: BrandController
 * @description:
 * @date: 2023/6/5 11:33
 * @since: 11
 */
@RestController
@RequestMapping("brand")
public class BrandController extends BaseController {

    @Autowired
    private BrandService brandService;


    /**
     * 查询所有
     */
    @GetMapping
    public AxiosResult<List<Brand>> list(){
        return AxiosResult.success(brandService.list());
    }

    /**
     * 根据id查询一条数据
     */
    @GetMapping("{id}")
    public AxiosResult<Brand> findById(@PathVariable Long id){
        return AxiosResult.success(brandService.findById(id));
    }

    /**
     * 添加品牌数据
     */
    @PostMapping
    public AxiosResult<Void> add(@RequestBody Brand brand){
        return toAxios(brandService.add(brand));
    }

    /**
     * 修改品牌数据
     */
    @PutMapping
    public AxiosResult<Void> update(@RequestBody Brand brand){
        return toAxios(brandService.update(brand));
    }

    /**
     * 删除一条数据
     */
    @DeleteMapping("{id}")
    public AxiosResult<Void> deleteById(@PathVariable Long id){
        return toAxios(brandService.deleteById(id));
    }


    /**
     * 删除多条数据
     */
    @DeleteMapping("batch/{ids}")
    public AxiosResult<Void> batchDeleteByIds(@PathVariable List<Long> ids){
        return toAxios(brandService.batchDeleteByIds(ids));
    }


}

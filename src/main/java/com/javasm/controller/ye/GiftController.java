package com.javasm.controller.ye;

import com.javasm.common.http.AxiosResult;
import com.javasm.controller.base.BaseController;
import com.javasm.entity.ye.YGift;
import com.javasm.service.ye.GiftService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//赠品管理
@RestController
public class GiftController extends BaseController {
    @Autowired
    private GiftService giftService;

    //查询所有
    @GetMapping("gift/queryAll")
    AxiosResult<List<YGift>> queryAll(){
        return AxiosResult.success(giftService.list());
    }


    //根据id查询
    @GetMapping("gift/queryId")
    AxiosResult<YGift> queryId(long id){
        return AxiosResult.success(giftService.findById(id));
    }

    //动态条件查询
    @GetMapping("gift/query")
    AxiosResult<List<YGift>> query(YGift yGift){
        return AxiosResult.success(giftService.findYGift(yGift));
    }

    // 商品转赠品动态条件查询
    @GetMapping("gift/queryYGoodsAndYGift")
    AxiosResult<List<YGift>> queryYGoodsAndYGift(YGift yGift){
        return AxiosResult.success(giftService.findYGoodsAndYGift(yGift));
    }
    //修改
    @PostMapping("gift/update")
    public AxiosResult<Void> update(@RequestBody YGift yGift){
        return toAxios(giftService.update(yGift));
    }

    //添加赠品
    @PostMapping("gift/add")
    public AxiosResult<Void> add(@RequestBody YGift yGift){
        return toAxios(giftService.add(yGift));
    }

    //删除赠品
    @DeleteMapping("gift/delete")
    public AxiosResult<Void> delete(Long id){
        return toAxios(giftService.deleteById(id));
    }

}

package com.javasm.controller.ye;

import com.javasm.common.http.AxiosResult;
import com.javasm.controller.base.BaseController;
import com.javasm.entity.ye.YProduct;
import com.javasm.service.ye.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductController extends BaseController {
    @Autowired
    private ProductService productService;

    //查询全部
    @GetMapping("product/queryAll")
    public AxiosResult<Object> demo01(){
        return AxiosResult.success(productService.list());
    }

    //产品动态条件查询
    @GetMapping("product/query")
    public AxiosResult<Object> demo02(YProduct yProduct){
        return AxiosResult.success(productService.findYProduct(yProduct));
    }

}

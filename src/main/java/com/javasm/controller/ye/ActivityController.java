package com.javasm.controller.ye;

import com.javasm.common.http.AxiosResult;
import com.javasm.controller.base.BaseController;
import com.javasm.entity.ye.YActivity;
import com.javasm.service.ye.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("activity")
public class ActivityController extends BaseController {

    @Autowired
    private ActivityService service;
    @GetMapping("condition")
    AxiosResult<List<YActivity>> conditionQuery(YActivity activity){
        return AxiosResult.success(service.conditionQuery(activity));
    }
}

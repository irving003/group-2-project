package com.javasm.controller.ye;

import com.javasm.common.http.AxiosResult;
import com.javasm.controller.base.BaseController;
import com.javasm.entity.ye.Brand;
import com.javasm.entity.ye.YSelected;
import com.javasm.entity.ye.YType;
import com.javasm.mapper.ye.SelectedMapper;
import com.javasm.service.ye.BrandService;
import com.javasm.service.ye.SelectedService;
import com.javasm.service.ye.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
@RestController
public class TypeController extends BaseController {
    @Autowired
    private TypeService typeService;

    @Autowired
    private BrandService brandService;

    @Autowired
    private SelectedService selectedService;

    //穿梭框查询
    @GetMapping("type/queryShuttleBox")
    AxiosResult<Object> queryShuttleBox(Long id){
        List<Object> list = new ArrayList<>();
        List<Brand> allBrand = brandService.list();
        YType type = typeService.findById(id);
        long id1 = type.getId();
        List<Brand> selectedBrand = selectedService.queryById(id1);
        list.add(allBrand);
        list.add(selectedBrand);
        return AxiosResult.success(list);
    }
    //查询所有
    @GetMapping("type/queryAll")
    AxiosResult<List<YType>> queryAll(){
        return AxiosResult.success(typeService.list());
    }

//     条件查询
    @GetMapping("type/query")
    AxiosResult<List<YType>> queryName(String name){
        return AxiosResult.success(typeService.queryName(name));
    }

    // 新增类型
    @GetMapping("type/add")
    AxiosResult<Void> add(YType yType){
        return toAxios(typeService.add(yType));
    }

    //修改
    @GetMapping("type/update")
    AxiosResult<Void> update(YType yType){
        return toAxios(typeService.update(yType));
    }

    //删除
    @GetMapping("type/delete")
    AxiosResult<Void> delete(Long id){
        return toAxios(typeService.deleteById(id));
    }



}

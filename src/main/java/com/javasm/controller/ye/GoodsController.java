package com.javasm.controller.ye;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.javasm.common.http.AxiosResult;
import com.javasm.controller.base.BaseController;
import com.javasm.entity.ye.YGoods;
import com.javasm.entity.ye.YProduct;
import com.javasm.service.ye.ProductService;
import com.javasm.service.ye.YGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//商品模块
@RestController
public class GoodsController extends BaseController {
    @Autowired
    private YGoodsService yGoodsService;

    @Autowired
    private ProductService productService;

    //查询所有
    @GetMapping("goods/queryAll")
    AxiosResult<List<YGoods>> queryAll(){
        return AxiosResult.success(yGoodsService.list());
    }

    //根据id查询
    @GetMapping("goods/queryId")
    AxiosResult<Object> queryId(long id){
        YGoods goods = yGoodsService.findById(id);
        long productCode = goods.getProductCode();
        YProduct product = productService.findByCoding(productCode);
        List<Object> list = new ArrayList<>();
        list.add(goods);
        list.add(product);
        return AxiosResult.success(list);
    }

    //商品等级条件查询
    @GetMapping("goods/queryGrade")
    AxiosResult<List<YGoods>> queryGrade(long grade){
       return AxiosResult.success(yGoodsService.queryGrade(grade));
    }


    //商品动态条件查询
    @PostMapping("goods/query")
    public AxiosResult<List<YGoods>> query(YGoods yGoods){
        List<YGoods> yGoods1 = yGoodsService.findYGoods(yGoods);
        return AxiosResult.success(yGoods1);
    }

    // 赠品转商品动态条件查询
    @PostMapping("goods/queryYGiftAndYGoods")
    AxiosResult<List<YGoods>> queryYGiftAndYGoods(YGoods yGoods){
        return AxiosResult.success(yGoodsService.findYGiftAndYGoods(yGoods));
    }



    //修改
    @PostMapping("goods/update")
    public AxiosResult<Void> update(@RequestBody YGoods yGoods){
        return toAxios(yGoodsService.update(yGoods));
    }

    //添加商品
    @PostMapping("goods/add")
    public AxiosResult<Void> add(@RequestBody YGoods yGoods){
        return toAxios(yGoodsService.add(yGoods));
    }

    //删除商品
    @PostMapping("goods/delete")
    public AxiosResult<Void> delete(Long id){
        return toAxios(yGoodsService.deleteById(id));
    }

    //批量删除
    @PostMapping("goods/deletes")
    public AxiosResult<Void> deletes(@RequestBody List<Long> l){
        int deletes = yGoodsService.deletes(l);
        System.out.println(deletes);
        return toAxios(deletes);
    }
}

package com.javasm.controller.ye;

import com.javasm.common.http.AxiosResult;
import com.javasm.controller.base.BaseController;
import com.javasm.entity.ye.YGoods;
import com.javasm.entity.ye.YSelected;
import com.javasm.service.ye.SelectedService;
import com.javasm.service.ye.YGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

// 穿梭框
@RestController
public class SelectedController extends BaseController {
    @Autowired
    private SelectedService selectedService;


    //将已选品牌加入到表中
    @PostMapping("selected/add")
    AxiosResult<Void> add(@RequestBody YSelected ySelected){
        return toAxios(selectedService.add(ySelected));
    }




}

package com.javasm.controller.ye;

import com.javasm.common.http.AxiosResult;
import com.javasm.controller.base.BaseController;
import com.javasm.entity.ye.YSupplier;
import com.javasm.service.ye.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
// 供应商模块

@RestController
public class SupplierController extends BaseController {
    @Autowired
    private SupplierService supplierService;

    //供应商动态条件查询
    @GetMapping("supplier/query")
    public AxiosResult<List<YSupplier>> query(YSupplier ySupplier){
        return AxiosResult.success(supplierService.findYSupplier(ySupplier));
    }

    //新增供应商
    @PostMapping("supplier/add")
    public AxiosResult<Void> add(@RequestBody YSupplier ySupplier){
        return toAxios(supplierService.add(ySupplier));
    }

    //修改
    @PutMapping("supplier/update")
    public AxiosResult<Void> update(@RequestBody YSupplier ySupplier){
        return toAxios(supplierService.update(ySupplier));
    }

    //删除
    @DeleteMapping("supplier/delete")
    public AxiosResult<Void> delete(Long id){
        return toAxios(supplierService.deleteById(id));
    }

    //批量删除
    @DeleteMapping("supplier/deleteAll/{ids}")
    public AxiosResult<Void> deleteAll(@PathVariable List<Long> ids){
        return toAxios(supplierService.batchDeleteByIds(ids));
    }
}

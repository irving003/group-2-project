package com.javasm.controller.ye;

import com.javasm.common.http.AxiosResult;
import com.javasm.controller.base.BaseController;
import com.javasm.entity.ye.YBinding;
import com.javasm.service.ye.BindingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BindingController extends BaseController {

    @Autowired
    private BindingService bindingService;

    @GetMapping("binding/query")
    AxiosResult<List<YBinding>> query(YBinding yBinding){
        return AxiosResult.success(bindingService.findYBinding(yBinding));
    }
}

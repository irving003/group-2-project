package com.javasm.controller.ye;

import com.javasm.common.http.AxiosResult;
import com.javasm.controller.base.BaseController;
import com.javasm.entity.ye.YTariff;
import com.javasm.service.ye.TariffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TariffController extends BaseController {
    @Autowired
    private TariffService tariffService;

    //查询所有
    @GetMapping("tariff/addAll")
    AxiosResult<List<YTariff>> addAll(){
        return AxiosResult.success(tariffService.list());
    }

    //根据ID查询
    @GetMapping("tariff/addId")
    AxiosResult<YTariff> addId(long id){
        return AxiosResult.success(tariffService.findById(id));
    }

    // 动态条件查询
    @GetMapping("tariff/query")
    AxiosResult<List<YTariff>> query(YTariff yTariff){
        return AxiosResult.success(tariffService.findYTariff(yTariff));
    }

    // 新增价格调整申请
    @PostMapping("tariff/add")
    AxiosResult<Void> add(@RequestBody YTariff yTariff){
        return toAxios(tariffService.add(yTariff));

    }

    // 修改
    @PostMapping("tariff/update")
    AxiosResult<Void> update(@RequestBody YTariff yTariff){
        return toAxios(tariffService.update(yTariff));
    }

    //根据ID删除
    @GetMapping("tariff/deleteId")
    AxiosResult<Void> deleteId(long id){
        return toAxios(tariffService.deleteById(id));
    }
}

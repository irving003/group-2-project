package com.javasm.controller.sys;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.javasm.common.config.LogConfig;
import com.javasm.common.http.AxiosResult;
import com.javasm.common.page.PageBean;
import com.javasm.controller.base.BaseController;
import com.javasm.entity.sys.SLog;
import com.javasm.entity.sys.SRole;
import com.javasm.entity.sys.SUser;
import com.javasm.service.sys.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("log")
public class LogController extends BaseController {

    @Autowired
    private LogService logService;

    @GetMapping
    @LogConfig(operationModule = "日志管理", description = "查询日志")
    public AxiosResult<List<SLog>> findAll() {
        return AxiosResult.success(logService.list());
    }
    @PostMapping("By")
    @LogConfig(operationModule = "日志管理", description = "条件查询日志")
    public AxiosResult<PageBean<SLog>> findBy(@RequestBody SLog sLog) {
        System.out.println(sLog);
        LambdaQueryWrapper<SLog> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(sLog.getLogModule()!=null&&!"".equals(sLog.getLogModule()),SLog::getLogModule,sLog.getLogModule())
                .between(sLog.getTime1()!=null&&sLog.getTime2()!=null,SLog::getUpdateTime,sLog.getTime1(),sLog.getTime2())
                .like(sLog.getLogName()!=null&&!"".equals(sLog.getLogName()),SLog::getLogName,sLog.getLogName());
        PageHelper.startPage(sLog.getPageInfo());
        List<SLog> search = logService.search(wrapper);

        PageInfo<SLog> pageInfo = new PageInfo<>(search);
        long total = pageInfo.getTotal();
        PageBean<SLog> sRolePageBean = PageBean.initData(total, search);
        return AxiosResult.success(sRolePageBean);
    }
}

package com.javasm.controller.sys;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.ICaptcha;
import cn.hutool.captcha.LineCaptcha;
import cn.hutool.captcha.ShearCaptcha;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.javasm.common.config.LogConfig;
import com.javasm.common.http.AxiosResult;
import com.javasm.common.page.PageBean;
import com.javasm.controller.base.BaseController;
import com.javasm.entity.sys.SUser;
import com.javasm.service.sys.UserService;
import com.javasm.util.JedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;
import redis.clients.jedis.Jedis;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("login")
public class LoginController extends BaseController {
    @Autowired
    private UserService userService;
    @Autowired
    private RedisTemplate redisTemplate;
    private Jedis jedis = JedisUtil.getJedis();




    @PostMapping("verify")
    @LogConfig(operationModule = "登录系统", description = "登录系统")
    public AxiosResult<List<SUser>> verify( HttpServletRequest request,@RequestBody Map<String, Object> map ) {
        String loginName = (String) map.get("loginName");
        String userPassword = (String) map.get("userPassword");
        String code = (String) map.get("code");
        LambdaQueryWrapper<SUser> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(loginName != null, SUser::getLoginName, loginName)
                .eq(userPassword != null, SUser::getUserPassword, userPassword);
        List<SUser> search = userService.search(wrapper);
        if (search.size() <= 0) {
            return AxiosResult.error();
        }
        if ("有效".equals(search.get(0).getUserStatus()) && code.equals(redisTemplate.opsForValue().get("code"))) {

            HttpSession session = request.getSession();
            redisTemplate.opsForValue().set("uid",search.get(0).getUserId());
            redisTemplate.opsForValue().set("username",search.get(0).getUserName());
            redisTemplate.opsForValue().set("roleId",search.get(0).getUserRoleId());
            //获取session中绑定的数据
//            System.out.println(getUidFromSession(session));
//            System.out.println(getUsernameFromSession(session));
//            System.out.println(getRoleIdFromSession(session));
//            System.out.println(session.getId());
//            System.out.println("---------------------------------------------------------------");

            redisTemplate.delete("code");
            return AxiosResult.success();
        }
        return AxiosResult.error();
    }
}

package com.javasm.controller.sys;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.javasm.common.config.LogConfig;
import com.javasm.common.http.AxiosResult;
import com.javasm.common.page.PageBean;
import com.javasm.controller.base.BaseController;
import com.javasm.entity.sys.SRole;
import com.javasm.entity.sys.SUser;
import com.javasm.service.sys.RoleService;
import com.javasm.service.sys.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("user")
public class UserController extends BaseController {
    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;


    @GetMapping("role")
    @LogConfig(operationModule="用户管理",description = "查询角色权限")
    public AxiosResult<List<String>> findrole() {
        List<SRole> srole = roleService.findAll();
        List<String> list = new ArrayList<>();
        srole.forEach(sRole -> {
            list.add(sRole.getRoleName());
        });
        return AxiosResult.success(list);
    }

    /**
     * 条件查询及分页
     */
    @PostMapping("search")
    @LogConfig(operationModule="用户管理",description = "条件查询权限")
    public AxiosResult<PageBean<SUser>> search(@RequestBody SUser sUser) {
        System.out.println(sUser);
        LambdaQueryWrapper<SUser> wrapper = new LambdaQueryWrapper<>();
        PageHelper.startPage(sUser.getPageInfo());
        wrapper.eq(sUser.getUserId() > 0, SUser::getUserId, sUser.getUserId())
        .eq(sUser.getLoginName() != null, SUser::getLoginName, sUser.getLoginName())
        .eq(sUser.getUserName() != null, SUser::getUserName, sUser.getUserName())
        .eq(sUser.getUserDepartment() != null, SUser::getUserDepartment, sUser.getUserDepartment())
        .eq(sUser.getUserPassword() != null, SUser::getUserPassword, sUser.getUserPassword())
        .eq(sUser.getUserSex() != null, SUser::getUserStatus, sUser.getUserStatus())
        .eq(sUser.getUserStatus() != null, SUser::getLoginName, sUser.getLoginName())
        .eq(sUser.getUserJob() != null, SUser::getUserJob, sUser.getUserJob())
        .eq(sUser.getUserPhone() != null, SUser::getUserPhone, sUser.getUserPhone())
        .eq(sUser.getUserEmail() != null, SUser::getUserEmail, sUser.getUserEmail())
        .eq(sUser.getUserRemarka() != null, SUser::getUserRemarka, sUser.getUserRemarka())
        .eq(sUser.getUserRemarkb() != null, SUser::getUserRemarkb, sUser.getUserRemarkb())
        .eq(sUser.getUserRemarkc() != null, SUser::getUserRemarkc, sUser.getUserRemarkc())
        .eq(sUser.getUserRemarkd() != null, SUser::getUserRemarkd, sUser.getUserRemarkd())
        .eq(sUser.getUserRemarke() != null, SUser::getUserRemarke, sUser.getUserRemarke())
        .eq(sUser.getUserRoleId() > 0, SUser::getUserRoleId, sUser.getUserRoleId());
        List<SUser> search = userService.search(wrapper);
        search.forEach(a->{
            SRole sRole = new SRole();
            sRole.setRoleId(a.getUserRoleId());
            a.setUserRoleName(roleService.findBysRole(sRole).get(0).getRoleName());
        });
        PageInfo<SUser> pageInfo = new PageInfo<SUser>(search);
        long total = pageInfo.getTotal();
        PageBean<SUser> pageBean = PageBean.initData(total, search);
        return AxiosResult.success(pageBean);
    }


    /**
     * 查询所有
     */
    @PostMapping("list")
    @LogConfig(operationModule="用户管理",description = "查询全部权限")
    public AxiosResult<PageBean<SUser>> findAll(@RequestBody PageInfo pageInfo) {
        PageHelper.startPage(pageInfo);
        List<SUser> all = userService.list();
        System.out.println(all);
        all.forEach(a -> {
            SRole sRole = new SRole();
            sRole.setRoleId(a.getUserRoleId());
            a.setUserRoleName(roleService.findBysRole(sRole).get(0).getRoleName());
        });
        PageInfo<SUser> userPageInfo = new PageInfo<>(all);
        long total = userPageInfo.getTotal();
        PageBean<SUser> sUserPageBean = PageBean.initData(total, all);
        return AxiosResult.success(sUserPageBean);
    }

    /**
     * 添加数据
     */
    @PostMapping("add")
    @LogConfig(operationModule="用户管理",description = "添加用户")
    public AxiosResult<Void> add(@RequestBody SUser sUser) {
        return toAxios(userService.add(sUser));
    }

    /**
     * 根据id查询
     */
    @GetMapping("{id}")
    @LogConfig(operationModule="用户管理",description = "根据id查询")
    public AxiosResult<SUser> findById(@PathVariable Long id) {
        return AxiosResult.success(userService.findById(id));
    }

    /**
     * 修改数据
     */
    @PutMapping("update")
    @LogConfig(operationModule="用户管理",description = "更新用户数据")
    public AxiosResult<Void> update(@RequestBody SUser sUser) {
        return toAxios(userService.update(sUser));
    }

    /**
     * 删除一条数据
     */
    @DeleteMapping("{id}")
    @LogConfig(operationModule="用户管理",description = "根据id删除")
    public AxiosResult<Void> deleteById(@PathVariable Long id) {
        return toAxios(userService.deleteById(id));
    }


    /**
     * 删除多条数据
     */
    @DeleteMapping("batch/{ids}")
    @LogConfig(operationModule="用户管理",description = "根据id删除多条数据")
    public AxiosResult<Void> batchDeleteByIds(@PathVariable List<Long> ids) {
        return toAxios(userService.batchDeleteByIds(ids));
    }
}

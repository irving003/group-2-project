package com.javasm.controller.sys;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import com.javasm.common.config.LogConfig;
import com.javasm.common.http.AxiosResult;
import com.javasm.common.http.AxiosStatus;
import com.javasm.common.page.PageBean;
import com.javasm.controller.base.BaseController;
import com.javasm.entity.sys.SAuthority;
import com.javasm.entity.sys.SOperation;
import com.javasm.entity.sys.SRole;
import com.javasm.entity.sys.SUser;
import com.javasm.service.sys.AuthorityService;
import com.javasm.service.sys.RoleService;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("authority")
public class AuthorityController extends BaseController{
    @Autowired
    private AuthorityService authorityService;
    @Autowired
    private RoleService roleService;
    // 直接注入
    @Autowired
    private RedisTemplate redisTemplate;


    /**
     * 查询全部权限
     */
    @GetMapping
    public AxiosResult<List<String>> findAll1() {
        List<SOperation> all = authorityService.findAllOperation(null);
        List<String> authorityName = new ArrayList<>();
        all.forEach(authority -> {
            authorityName.add(authority.getOperationName());
        });
        return AxiosResult.success(authorityName);
    }

    /**
     * 查询权限可执行的操作
     */
    @LogConfig(operationModule = "权限管理", description = "查询权限可执行的操作")
    public List<SAuthority> findOperation(List<SAuthority> allAuthority) {
        allAuthority.forEach(authority -> {

            List<SOperation> allOperation = authorityService.findAllOperation((int) authority.getAuthorityId());
            List<String> str = new ArrayList<>();
            allOperation.forEach(sOperation -> {
                str.add(sOperation.getOperationName());
            });
            String s = str.toString();
            authority.setOperation(s);
            authority.setOperationName(allOperation);
        });
        return allAuthority;
    }

    /**
     * 查询全部
     */
//    @PostMapping
//    public AxiosResult<PageBean<SAuthority>> findAll(@RequestBody PageInfo pageInfo) {
//        PageHelper.startPage(pageInfo);
//        List<SAuthority> allAuthority = authorityService.findAll();
//        PageInfo<SAuthority> pageInfo1 = new PageInfo<>(allAuthority);
//        long total = pageInfo1.getTotal();
//        List<SAuthority> operation = findOperation(allAuthority);
//        return AxiosResult.success(PageBean.initData(total,operation));
//    }
    @PostMapping("findAll")
    @LogConfig(operationModule = "权限管理", description = "查询全部权限")
    public AxiosResult<List<SAuthority>> findAll(HttpServletRequest request) {
            long roleId = (long) redisTemplate.opsForValue().get("roleId");
            List<SAuthority> authorityById = roleService.findAuthorityById((int) roleId);

            return AxiosResult.success(findOperation(authorityById));

    }

    /**
     * 条件查询
     */
    @PostMapping("findBy")
    @LogConfig(operationModule = "权限管理", description = "条件查询全部权限")
    public AxiosResult<PageBean<SAuthority>> findBy(@RequestBody SAuthority sAuthority) {
        PageHelper.startPage(sAuthority.getPageInfo());
        List<SAuthority> allAuthority = authorityService.findBysAuthority(sAuthority);

        PageInfo<SAuthority> pageInfo1 = new PageInfo<>(allAuthority);
        long total = pageInfo1.getTotal();
        List<SAuthority> operation = findOperation(allAuthority);
        return AxiosResult.success(PageBean.initData(total, operation));
    }

    /**
     * 添加角色
     */
    @PostMapping("add")
    @LogConfig(operationModule = "权限管理", description = "添加权限")
    public AxiosResult<Void> add(@RequestBody SAuthority sAuthority) {
        return toAxios(authorityService.add(sAuthority));
    }

    /**
     * 修改角色
     */
    @PostMapping("update")
    @LogConfig(operationModule = "权限管理", description = "修改权限或修改权的可执行操作")
    public AxiosResult<Void> update(@RequestBody SAuthority sAuthority) {
        return toAxios(authorityService.update(sAuthority));
    }

    /**
     * 删除角色
     */
    @PostMapping("delete")
    @LogConfig(operationModule = "权限管理", description = "删除权限")
    public AxiosResult<Void> deleteAuthority(@RequestBody SAuthority sAuthority) {
        return toAxios(authorityService.deleteAuthority(sAuthority.getAuthorityId()));
    }

    /**
     * 权限报表
     */
    @PostMapping("report")
    @LogConfig(operationModule = "角色权限", description = "查询所有角色权限")
    public AxiosResult<PageBean<SUser>> findReport(@RequestBody SUser user) {
        PageHelper.startPage(user.getPageInfo());
        List<SUser> report = authorityService.findReport(user);
        List<SUser> sUsers = addReport(report);
        PageInfo<SUser> pageInfo1 = new PageInfo<>(sUsers);
        long total = pageInfo1.getTotal();
        return AxiosResult.success(PageBean.initData(total, sUsers));
    }

    public List<SUser> addReport(List<SUser> report) {
        //        向用户里面添加权限和操作内容
        report.forEach(r -> {
//            查询角色名称
            SRole sRole = new SRole();
            sRole.setRoleId(r.getUserRoleId());
            List<SRole> bysRole = roleService.findBysRole(sRole);
            r.setUserRoleName(bysRole.get(0).getRoleName());
//            查询权限
            List<SAuthority> authorityList = roleService.findAuthorityById((int) r.getUserRoleId());
            r.setAuthorityName(authorityList);
            List<String> authorities = new ArrayList<>();
            List<String> operations = new ArrayList<>();

            List<SOperation> sOperations = new ArrayList<>();
            authorityList.forEach(a -> {
//                查询可执行的操作
                System.out.println(a.toString());
                List<SOperation> allOperation = authorityService.findAllOperation((int) a.getAuthorityId());
                allOperation.forEach(o -> {
                    operations.add(o.getOperationName());
                    sOperations.add(o);
                });
//                权限名称
                authorities.add(a.getAuthorityName());
            });
            r.setOperation(operations.toString());
            r.setAuthority(authorities.toString());
            r.setOperationName(sOperations);
        });
        return report;
    }

    /**
     * 权限移出
     */
    @PostMapping("deleteAuthority")
    @LogConfig(operationModule = "角色权限", description = "查询所有角色权限")
    public AxiosResult<Void> deleteAuthorityById(@RequestBody SUser s) {
        return toAxios(roleService.deleteRoleAuthority(s.getUserRoleId()));
    }

    /**
     * 权限导出查询 不分页
     */
    public List<SUser> report() {
        List<SUser> report = authorityService.findReport(null);
        List<SUser> sUsers = addReport(report);
        return sUsers;
    }

    /**
     * 权限导出
     */
    @PostMapping("reportOut")
    @LogConfig(operationModule = "角色权限", description = "权限导出不分页")
    public AxiosResult<Void> reportDerive() throws IOException {
        List<SUser> report = report();
//        创建工作薄
        XSSFWorkbook workbook = new XSSFWorkbook();
//        创建页
        Sheet sheet = workbook.createSheet("用户权限报表");
        Row row1 = sheet.createRow(0);
        List<String> strings = Arrays.asList("用户id", "登录名", "用户名", "用户部门", "用户性别", "用户状态", "用户职务", "用户电话", "用户邮件",
                "备注1", "备注2", "备注3", "备注4", "备注5", "用户角色", "用户权限", "用户权限范围");
        for (int i = 0; i < strings.size(); i++) {
            Cell cell = row1.createCell(i);
            cell.setCellValue(strings.get(i));
        }
//        创建行
        for (int i = 0; i < report.size(); i++) {
            Row row = sheet.createRow(i + 1);
            SUser sUser = report.get(i);
            Cell idCell = row.createCell(0);
            idCell.setCellValue(sUser.getUserId());
            Cell loginNameCell = row.createCell(1);
            loginNameCell.setCellValue(sUser.getLoginName());
            Cell userNameCell = row.createCell(2);
            userNameCell.setCellValue(sUser.getUserName());
            Cell userDepartmentCell = row.createCell(3);
            userDepartmentCell.setCellValue(sUser.getUserDepartment());
            Cell userSexCell = row.createCell(4);
            userSexCell.setCellValue(sUser.getUserSex());
            Cell userStatusCell = row.createCell(5);
            userStatusCell.setCellValue(sUser.getUserStatus());
            Cell userJobCell = row.createCell(7);
            userJobCell.setCellValue(sUser.getUserJob());
            Cell userPhoneCell = row.createCell(8);
            userPhoneCell.setCellValue(sUser.getUserPhone());
            Cell userEmailCell = row.createCell(9);
            userEmailCell.setCellValue(sUser.getUserEmail());
            Cell userRemarkaCell = row.createCell(10);
            userRemarkaCell.setCellValue(sUser.getUserRemarka());
            Cell userRemarkbCellCell = row.createCell(11);
            userRemarkbCellCell.setCellValue(sUser.getUserRemarkb());
            Cell userRemarkcCell = row.createCell(12);
            userRemarkcCell.setCellValue(sUser.getUserRemarkc());
            Cell userRemarkdCell = row.createCell(13);
            userRemarkdCell.setCellValue(sUser.getUserRemarkd());
            Cell userRemarkeCell = row.createCell(14);
            userRemarkeCell.setCellValue(sUser.getUserRemarke());
            Cell userRoleNameCell = row.createCell(15);
            userRoleNameCell.setCellValue(sUser.getUserRoleName());
            Cell authorityCell = row.createCell(16);
            authorityCell.setCellValue(sUser.getAuthority());
            Cell operationCell = row.createCell(17);
            operationCell.setCellValue(sUser.getOperation());
        }
        // 创建文件输出流
        FileOutputStream fileOutputStream = new FileOutputStream("d://demo.xlsx");
        workbook.write(fileOutputStream);
//        写到内存中
//            ByteArrayOutputStream stream = new ByteArrayOutputStream();
//            workbook.write(stream);
//            byte[] bytes = stream.toByteArray();
//            响应头
//            HttpHeaders httpHeaders = new HttpHeaders();
//            httpHeaders.setContentDispositionFormData("attachment", URLEncoder.encode("用户权限报表","utf-8"));
        return AxiosResult.success();
    }


}

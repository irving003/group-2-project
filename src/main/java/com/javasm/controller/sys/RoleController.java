package com.javasm.controller.sys;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.javasm.common.config.LogConfig;
import com.javasm.common.http.AxiosResult;
import com.javasm.common.page.PageBean;
import com.javasm.controller.base.BaseController;
import com.javasm.entity.sys.SAuthority;
import com.javasm.entity.sys.SRole;
import com.javasm.service.sys.AuthorityService;
import com.javasm.service.sys.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("role")
public class RoleController extends BaseController {
    @Autowired
    private RoleService roleService;
    @Autowired
    private AuthorityService authorityService;
    /**
     * 查询全部权限
     */
    @GetMapping("allAuthority")
    @LogConfig(operationModule="角色管理",description = "查询全部权限")
    public  AxiosResult<List<String>> findAll() {
        List<SAuthority> all = authorityService.findAll();
        List<String> authorityName = new ArrayList<>();
        all.forEach(authority -> {
            authorityName.add(authority.getAuthorityName());
        });
        return AxiosResult.success(authorityName);
    }

    /**
     * 查询权限
     */
    @LogConfig(operationModule="角色管理",description = "查询角色的权限")
    public List<SRole> findAuthority(List<SRole> roleList) {
        roleList.forEach(sRole -> {

            List<SAuthority> authorityList = roleService.findAuthorityById((int) sRole.getRoleId());
            List<String> str = new ArrayList<>();
            authorityList.forEach(sAuthority -> {
                str.add(sAuthority.getAuthorityName());
            });
            String s = str.toString();
            sRole.setAuthority(s);
            sRole.setAuthorityName(authorityList);
        });
        return roleList;
    }

    /**
     * 查询全部
     */
    @PostMapping
    @LogConfig(operationModule="角色管理",description = "查询全部角色")
    public AxiosResult<PageBean<SRole>> findAll(@RequestBody PageInfo pageInfo) {
        PageHelper.startPage(pageInfo);
        List<SRole> roleList = roleService.findAll();
        PageInfo<SRole> page = new PageInfo<>(roleList);
        long total = page.getTotal();
        List<SRole> authority = findAuthority(roleList);
        PageBean<SRole> sRolePageBean = PageBean.initData(total, authority);
        if (sRolePageBean==null){
            return AxiosResult.error();
        }
        return AxiosResult.success(sRolePageBean);
    }

    /**
     * 条件查询
     */
    @PostMapping("findBy")
    @LogConfig(operationModule="角色管理",description = "条件查询角色")
    public AxiosResult<PageBean<SRole>> findBy(@RequestBody SRole sRole) {

        PageHelper.startPage(sRole.getPageInfo());
        List<SRole> roleList = roleService.findBysRole(sRole);
        PageInfo<SRole> pageInfo = new PageInfo<>(roleList);
        long total = pageInfo.getTotal();
        List<SRole> authority = findAuthority(roleList);
        PageBean<SRole> sRolePageBean = PageBean.initData(total, authority);
        if (sRolePageBean==null){
            return AxiosResult.error();
        }
        return AxiosResult.success(sRolePageBean);
    }

    /**
     * 添加角色
     */
    @PostMapping("add")
    @LogConfig(operationModule="角色管理",description = "添加角色")
    public AxiosResult<Void> add(@RequestBody SRole sRole) {

        return toAxios(roleService.add(sRole));
    }

    /**
     * 修改角色
     */
    @PostMapping("update")
    @LogConfig(operationModule="角色管理",description = "修改角色或修改角色的权限")
    public AxiosResult<Void> update(@RequestBody SRole sRole) {
        return toAxios(roleService.update(sRole));
    }
    /**
     * 删除角色
     */
    @PostMapping("delete")
    @LogConfig(operationModule="角色管理",description = "删除角色并删除角色的权限关联表")
    public AxiosResult<Void> delete(@RequestBody SRole sRole) {
        return toAxios(roleService.delete(sRole.getRoleId()));
    }

}

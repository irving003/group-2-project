package com.javasm.controller.base;

import com.javasm.common.http.AxiosResult;
import com.javasm.common.page.PageBean;
import com.javasm.entity.B2C.Order;

/**
 * @author: Author
 * @className: BaseController
 * @description:
 * @date: 2023/6/5 14:57
 * @since: 11
 */
public class BaseController {

    protected AxiosResult<Void> toAxios(int row){
        return row > 0 ? AxiosResult.success() : AxiosResult.error();
    }


}

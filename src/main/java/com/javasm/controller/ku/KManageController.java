package com.javasm.controller.ku;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.javasm.common.http.AxiosResult;

import com.javasm.common.page.PageBean;
import com.javasm.controller.base.BaseController;
import com.javasm.entity.ku.KManage;
import com.javasm.service.ku.KManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("manage")
public class KManageController extends BaseController {

    @Autowired
    private KManageService kManageService;

    /**
     * 查询所有
     */
    @GetMapping("query")
    public AxiosResult<List<KManage>> list() {
        return AxiosResult.success(kManageService.list());
    }

    /**
     * 根据id查询一条数据
     */
    @GetMapping("{id}")
    public AxiosResult<KManage> findById(@PathVariable Long id) {
        return AxiosResult.success(kManageService.findById(id));
    }

    /**
     * 查询
     */
    @PostMapping("manages")
    public AxiosResult<PageBean> KManage(@RequestBody Map<String, String> map) {
        QueryWrapper<KManage> queryWrapper = new QueryWrapper<>();
        queryWrapper.like(map.get("shoppingId") != null && !"".equals(map.get("shoppingId")), "shopping_id",
                map.get("shoppingId"))
                .eq(map.get("shoppingType") != null && !"".equals(map.get("shoppingType")), "shopping_type", map.get(
                        "shoppingType"))
                .eq(map.get("suppliersName") != null && !"".equals(map.get("suppliersName")), "suppliers_name", map.get("suppliersName"))
                .like(map.get("suppliersId") != null && !"".equals(map.get("suppliersId")), "suppliers_id", map.get("suppliersId"))
                .between(map.get("shoppingDate") != null && !"".equals(map.get("shoppingDate")) && map.get("shoppingDate1") != null && !""
                        .equals(map.get("shoppingDate1")), "shopping_date", map.get("shoppingDate"), map.get("shoppingDate1"));
        PageHelper.startPage(Integer.parseInt(map.get("pageNum")), Integer.parseInt(map.get("pageSize")));
        List<KManage> kManages = kManageService.search(queryWrapper);
        PageInfo<KManage> kManagePageInfo = new PageInfo<>(kManages);
        PageBean<KManage> pageBean = PageBean.initData(kManagePageInfo.getTotal(), kManages);
        return AxiosResult.success(pageBean);
    }

}

//    /**
//     * 添加申诉管理数据
//     */
//    @PostMapping
//    public AxiosResult<Void> add(@RequestBody KManage kManage){
//        return toAxios(kManageService.add(kManage));
//    }

//    /**
//     * 修改申诉管理数据
//     */
//    @PutMapping
//    public AxiosResult<Void> update(@RequestBody KManage kManage){
//        return toAxios(kManageService.update(kManage));
//    }

//    /**
//     * 删除一条数据
//     */
//    @DeleteMapping("{id}")
//    public AxiosResult<Void> deleteById(@PathVariable Long id){
//        return toAxios(kManageService.deleteById(id));
//    }
//
//
//    /**
//     * 删除多条数据
//     */
//    @DeleteMapping("batch/{ids}")
//    public AxiosResult<Void> batchDeleteByIds(@PathVariable List<Long> ids){
//        return toAxios(kManageService.batchDeleteByIds(ids));
//    }




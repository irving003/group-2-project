package com.javasm.controller.B2C;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.javasm.common.http.AxiosResult;
import com.javasm.controller.base.BaseController;
import com.javasm.entity.B2C.BConsignee;
import com.javasm.service.B2C.ConsigneeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("consignee")
public class ConsigneeController extends BaseController {
    @Autowired
    private ConsigneeService consigneeService;

    @GetMapping
    public AxiosResult<List<BConsignee>> list() {
        return AxiosResult.success(consigneeService.list());
    }

    @GetMapping("findByOId/{orderId}")
    public AxiosResult<BConsignee> findByOId(@PathVariable Long orderId) {
        LambdaQueryWrapper<BConsignee> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(orderId != null, BConsignee::getOrderId, orderId);
        BConsignee consignee = consigneeService.findByOId(wrapper);
        return AxiosResult.success(consignee);
    }
}

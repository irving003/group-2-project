package com.javasm.controller.B2C;


import com.javasm.common.http.AxiosResult;
import com.javasm.common.page.PageBean;
import com.javasm.common.page.PageQuery;
import com.javasm.common.tools.ExportExcelUtil;
import com.javasm.controller.base.BaseController;
import com.javasm.entity.B2C.BConsignee;
import com.javasm.entity.B2C.Order;
import com.javasm.entity.B2C.OrderQueryForm;
import com.javasm.entity.ye.Brand;
import com.javasm.entity.ye.YGoods;
import com.javasm.service.B2C.OrderService;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.util.List;


@RestController
@RequestMapping("order")
public class OrderController extends BaseController {

    @Autowired
    private OrderService orderService;



    @PostMapping
    public AxiosResult<Void> add(@RequestBody Order order) {
        return toAxios(orderService.add(order));
    }

    /**
     * 条件查询及分页
     */
    @PostMapping("search")
    public AxiosResult<PageBean<Order>> search(@RequestBody OrderQueryForm queryForm) {
        System.out.println(queryForm.getPageInfo());
        List<Order> search = orderService.search(queryForm);
        return PageQuery.search(search);
    }

    @GetMapping("findByOId/{OId}")
    public AxiosResult<Order> findById(@PathVariable Long OId) {
        return AxiosResult.success(orderService.findFormData(OId));
    }

    /**
     * 修改订单状态
     */
    @PutMapping
    public AxiosResult<Void> updateOState(@RequestBody Order order) {

        return toAxios(orderService.update(order));
    }

    /**
     * 删除单个
     */
//    @DeleteMapping("{id}")
//    public AxiosResult<Void> delete(@PathVariable Long id){
//        return toAxios(orderService.deleteById(id));
//    }

    /**
     * 批量删除
     */
    @DeleteMapping("batch/{ids}")
    public AxiosResult<Void> batchDelete(@PathVariable List<Long> ids) {
        return toAxios(orderService.batchDeleteByIds(ids));
    }

    /**
     * 导入
     */



    /**
     * 生成销售出库单
     * 问题：缓存影响
     */
    @GetMapping("export/{orderIds}")
    public void export(@PathVariable List<Long> orderIds, HttpServletRequest request, HttpServletResponse response) {

        for (Long orderId : orderIds) {
            //excel表名
            String title = "销售出库单";
            //excel标题
            String[] tittles = {"订单基本资料", "收货人资料", "订单商品信息"};

            String[] headers = {"订单编号", "下单日期", "订单类型", "订单动作", "业务类型", "订单状态",
                    "收货人姓名", "会员账号", "联系电话", "联系手机", "邮政编码", "收货地址", "配送地区", "配送方式", "配送费用", "支付方式",
                    "自提时间", "电子邮箱", "汇款人", "备注", "商品编号", "商品名称", "单价", "数量", "总金额"};
            //excel文件名
            String fileName = title + System.currentTimeMillis() + ".xls";

            //模拟数据
            String[] values = new String[headers.length - 5];
            Order formData = orderService.findFormData(orderId);
            int index = 0;
            values[index++] = formData.getOrderId().toString();
            values[index++] = formData.getOrderTime().toString();
            values[index++] = formData.getOrderType();
            values[index++] = formData.getOrderAction();
            values[index++] = formData.getBusType();
            values[index++] = formData.getOrderState();
            BConsignee consignee = formData.getbConsignee();
            values[index++] = consignee.getName();
            values[index++] = consignee.getAccount();
            values[index++] = consignee.getPhoneNum();
            values[index++] = consignee.getPhone();
            values[index++] = consignee.getPostcode();
            values[index++] = consignee.getAddress();
            values[index++] = consignee.getShipArea();
            values[index++] = consignee.getShipMode();
            values[index++] = String.valueOf(consignee.getShipCost());
            values[index++] = consignee.getPayMode();
            values[index++] = consignee.getPickTime().toString();
            values[index++] = consignee.getEmail();
            values[index++] = consignee.getRemitter();
            values[index++] = consignee.getComment();


            List<YGoods> goodsList = formData.getGoods();
            String[][] values1 = new String[goodsList.size()][5];
            for (int i = 0; i < goodsList.size(); i++) {
                values1[i][0] = String.valueOf(goodsList.get(i).getId());
                values1[i][1] = goodsList.get(i).getGoodsName();
                values1[i][2] = String.valueOf(goodsList.get(i).getGoodsPrice());
                values1[i][3] = String.valueOf(goodsList.get(i).getGoodsNumber());
                values1[i][4] = String.valueOf(goodsList.get(i).getGoodsPrice() * goodsList.get(i).getGoodsNumber());
            }

            HSSFWorkbook wb = ExportExcelUtil.getHSSFWorkbook(title, tittles, headers, values, values1);

            try {
                this.setResponseHeader(response, fileName);
                OutputStream os = response.getOutputStream();
                wb.write(os);
                os.flush();
                os.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public void setResponseHeader(HttpServletResponse response, String fileName) {
        try {
            try {
                fileName = new String(fileName.getBytes(), "ISO8859-1");
            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            response.setContentType("application/octet-stream;charset=ISO8859-1");
            response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
            response.addHeader("Pargam", "no-cache");
            response.addHeader("Cache-Control", "no-cache");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}

package com.javasm.controller.aspect;

import com.javasm.common.config.LogConfig;
import com.javasm.controller.base.BaseController;

import com.javasm.entity.sys.SLog;
import com.javasm.entity.sys.SUser;
import com.javasm.service.sys.LogService;
import com.javasm.util.JedisUtil;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.HandlerInterceptor;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.lang.reflect.Method;
import java.time.LocalDateTime;


@Component
@Aspect
public class AspectController  implements HandlerInterceptor {
    @Autowired
    private LogService logService;

    @Autowired
    private RedisTemplate redisTemplate;

    @Pointcut("execution(* com.javasm.controller..*.*(..)))")
    public void pc() {
    }

    @After("pc()")
    public void after(JoinPoint joinPoint) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        LogConfig logConfig = method.getAnnotation(LogConfig.class);
        String operationModule = "";
        String description = "";
        if (logConfig != null) {
            operationModule = logConfig.operationModule() != null && !"".equals(logConfig.operationModule()) ? logConfig.operationModule() : "未知";
            description = logConfig.description() != null && !"".equals(logConfig.description()) ? logConfig.description() : "未知";
        }
        //获取请求的类名
        String className = joinPoint.getTarget().getClass().getName();
        //获取请求的方法名
        String methodName = method.getName();
        //获取当前用户名
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        Object principal = authentication.getPrincipal();
//        if(principal!=null){
//            SLog u = (SLog)principal;
//            String logName = u.getLogName();
//            System.out.println(logName);
//        }

        //保存日志
        SLog sLog = new SLog();
        sLog.setUpdateTime(LocalDateTime.now());
        sLog.setLogModule(operationModule);
        sLog.setLogContent(description);
        sLog.setLogName(redisTemplate.opsForValue().get("username") != null ? (String) redisTemplate.opsForValue().get(
                "username") : "未知");
//        sLog.setLogName(JedisUtil.getJedis().get("loginName") != null ? JedisUtil.getJedis().get("loginName") : "未知");
        if (sLog != null) {
            logService.add(sLog);
        }
    }


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        Object obj = redisTemplate.opsForValue().get("uid");
        System.out.println(obj);
        if (obj == null) {
            //说明用户没有登陆过系统，重定向到界面

            response.sendError(40000);
//            request.getRequestDispatcher("/login").forward(request, response);
            //结束后续的调用

            return false;
        }
        //请求放行
        return true;
    }

//    public boolean preHandle(HttpServletResponse response ) throws Exception {
//        // HttpServletRequest对象来获取session对象
//
//             Object obj = request.getSession().getAttribute("uid");
//            if (obj == null) {
//                //说明用户没有登陆过系统，重定向到界面
//
//                response.sendError(40000);
////            request.getRequestDispatcher("/login").forward(request, response);
//                //结束后续的调用
//
//                return false;
//            }
//            //请求放行
//            return true;
//        }



}

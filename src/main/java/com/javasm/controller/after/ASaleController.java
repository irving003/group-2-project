package com.javasm.controller.after;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.javasm.common.config.LogConfig;
import com.javasm.common.http.AxiosResult;
import com.javasm.common.page.PageBean;
import com.javasm.controller.base.BaseController;
import com.javasm.entity.after.ARepair;
import com.javasm.entity.after.ASale;


import com.javasm.entity.after.ASaleImei;
import com.javasm.entity.ye.YGoods;


import com.javasm.service.after.ARepairService;
import com.javasm.service.after.ASaleImeiService;
import com.javasm.service.after.ASaleService;
import com.javasm.service.ye.YGoodsService;
import com.javasm.util.JedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @program: group-2-project
 * @description:
 * @author: lydms
 * @create: 2023-06-06 09:38
 **/
@RestController
@RequestMapping("asale")
public class ASaleController extends BaseController {

    @Autowired
    private ASaleService aSaleService;
    @Autowired
    private YGoodsService yGoodsService;
    @Autowired
    private ARepairService aRepairService;
    @Autowired
    private ASaleImeiService aSaleImeiService;
    @Autowired
    private RedisTemplate redisTemplate;


    //    查询全部
    @GetMapping
    public AxiosResult<List<ASale>> list() {
        return AxiosResult.success(aSaleService.list());
    }

    //  根据id查询
    @GetMapping("{id}")
    public AxiosResult<ASale> findById(@PathVariable Long id) {
        return AxiosResult.success(aSaleService.findById(id));
    }

    //添加
    @Transactional
    @PostMapping
    public AxiosResult<Void> add(@RequestBody ASale aSale ){
        if (aSale.getId()!=null){
            aSaleImeiService.deleteByOutCode(aSale.getOutCode());
            for (YGoods getyGood : aSale.getyGoods()) {
                aSaleImeiService.add(new ASaleImei(aSale.getOutCode(),getyGood.getImei()));
            }
            return toAxios(aSaleService.update(aSale));
        }
        Long outCode = Long.valueOf((String) redisTemplate.opsForValue().get("outCode"))+1;
        aSale.setOutCode(outCode);
        redisTemplate.opsForValue().set("outCode",outCode+"");
        for (YGoods getyGood : aSale.getyGoods()) {
            aSaleImeiService.add(new ASaleImei(aSale.getOutCode(),getyGood.getImei()));
        }
        return toAxios(aSaleService.add(aSale));
    }

//    修改
    @PutMapping
    public AxiosResult<Void> update(@RequestBody ASale aSale){
        return toAxios(aSaleService.update(aSale));
    }

//   删除
    @Transactional
    @DeleteMapping("{id}")
    public AxiosResult<Void> deleteById(@PathVariable Long id){
        ASale aSale = aSaleService.findById(id);
        aSaleImeiService.deleteByOutCode(aSale.getOutCode());
        return toAxios(aSaleService.deleteById(id));
    }

//条件查询返厂出库单
@LogConfig(operationModule = "返厂出库单管理", description = "返厂出库单管理")
    @PostMapping ("asales")
    public AxiosResult<PageBean> ASales(@RequestBody Map<String,String> map){
        QueryWrapper<ASale> queryWrapper = new QueryWrapper<>();
        queryWrapper.like(map.get("outCode")!=null&&!"".equals(map.get("outCode")),"out_code",map.get("outCode"))
                .eq(map.get("backMark")!=null&&!"".equals(map.get("backMark")),"back_mark",map.get("backMark"))
                .eq(map.get("auditState")!=null&&!"".equals(map.get("auditState")),"audit_state",map.get("auditState"))
                .eq(map.get("createBy")!=null&&!"".equals(map.get("createBy")),"create_by",map.get("createBy"))
                .between(map.get("createTime")!=null&&!"".equals(map.get("createTime"))&&map.get("createTime1")!=null&&!"".equals(map.get("createTime1")),"create_time",map.get("createTime"), map.get("createTime1"))
                .between(map.get("auditTime")!=null&&!"".equals(map.get("auditTime"))&&map.get("auditTime1")!=null&&!"".equals(map.get("auditTime1")),"audit_time",map.get("auditTime"),map.get("auditTime1"));
        PageHelper.startPage(Integer.parseInt(map.get("pageNum")), Integer.parseInt(map.get("pageSize")));
        List<ASale> aSales = aSaleService.search(queryWrapper);
        List<YGoods> imeis=new ArrayList<>();
        for (ASale aSale : aSales) {
            List<ASaleImei> outCode = aSaleImeiService.search(new QueryWrapper<ASaleImei>().eq("out_code", aSale.getOutCode()));
            for (ASaleImei aSaleImei : outCode) {

                List<YGoods> imei = yGoodsService.search(new QueryWrapper<YGoods>().eq("imei", aSaleImei.getImei()));
                System.out.println(imei);
                imeis.addAll(imei);
            }
            aSale.setyGoods(imeis);
            imeis=new ArrayList<>();
        }
        PageInfo<ASale> aSalePageInfo = new PageInfo<>(aSales);
        PageBean<ASale> pageBean = PageBean.initData(aSalePageInfo.getTotal(),aSales);
        return AxiosResult.success(pageBean);
    }
//    添加维修点
    @PostMapping("aRepair")
    public AxiosResult<Void> addARepair(@RequestBody ARepair aRepair){
        return toAxios(aRepairService.add(aRepair));
    }
    @PutMapping("aRepair")
    public AxiosResult<Void> updateARepair(@RequestBody ARepair aRepair){
        return toAxios(aRepairService.update(aRepair));
    }

}

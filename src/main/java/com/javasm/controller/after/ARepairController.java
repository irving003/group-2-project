package com.javasm.controller.after;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.javasm.common.http.AxiosResult;
import com.javasm.common.page.PageBean;
import com.javasm.controller.base.BaseController;
import com.javasm.entity.after.ARepair;
import com.javasm.entity.after.ASale;
import com.javasm.service.after.ARepairService;
import com.javasm.util.JedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import redis.clients.jedis.Jedis;

import java.util.List;
import java.util.Map;

/**
 * @program: group-2-project
 * @description:
 * @author: lydms
 * @create: 2023-06-09 10:00
 **/
@RestController
@RequestMapping("aRepair")
public class ARepairController extends BaseController {

    @Autowired
    private ARepairService aRepairService;
//条件查询
    @RequestMapping("aRepairs")
    public AxiosResult<PageBean> ARepairs(@RequestBody Map<String,String> map){
        QueryWrapper<ARepair> queryWrapper = new QueryWrapper<>();
        queryWrapper.like(map.get("companyName")!=null&&!"".equals(map.get("companyName")),"company_name",
                        map.get("companyName"))
                .like(map.get("companyAddress")!=null&&!"".equals(map.get("companyAddress")),"company_address",
                        map.get("companyAddress"))
                .like(map.get("phone")!=null,"phone",map.get("phone"));
        PageHelper.startPage(Integer.parseInt(map.get("pageNum")), Integer.parseInt(map.get("pageSize")));
        List<ARepair> aRepairs = aRepairService.search(queryWrapper);
        PageInfo<ARepair> aRepairPageInfo = new PageInfo<>(aRepairs);
        PageBean<ARepair> pageBean = PageBean.initData(aRepairPageInfo.getTotal(),aRepairs);
        return AxiosResult.success(pageBean);
    }
    //    查询全部
    @GetMapping
    public AxiosResult<List<ARepair>> list() {
        return AxiosResult.success(aRepairService.list());
    }

    //  根据id查询
    @GetMapping("{id}")
    public AxiosResult<ARepair> findById(@PathVariable Long id) {
        return AxiosResult.success(aRepairService.findById(id));
    }

    //添加
    @PostMapping
    public AxiosResult<Void> add(@RequestBody ARepair aRepair){
        return toAxios(aRepairService.add(aRepair));
    }

    //    修改
    @PutMapping
    public AxiosResult<Void> update(@RequestBody ARepair aRepair){
        return toAxios(aRepairService.update(aRepair));
    }

    //   删除
    @DeleteMapping("{id}")
    public AxiosResult<Void> deleteById(@PathVariable Long id){
        return toAxios(aRepairService.deleteById(id));
    }

}

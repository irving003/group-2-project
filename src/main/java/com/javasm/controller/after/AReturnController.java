package com.javasm.controller.after;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.javasm.common.http.AxiosResult;
import com.javasm.common.page.PageBean;
import com.javasm.controller.base.BaseController;
import com.javasm.entity.B2C.BConsignee;
import com.javasm.entity.B2C.Order;


import com.javasm.entity.after.ARepair;
import com.javasm.entity.after.AReturn;


import com.javasm.entity.after.ASaleImei;
import com.javasm.entity.ye.YGoods;
import com.javasm.service.B2C.ConsigneeService;
import com.javasm.service.B2C.OrderService;


import com.javasm.service.after.AReturnService;
import com.javasm.service.after.ASaleImeiService;
import com.javasm.service.ye.YGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @program: group-2-project
 * @description:
 * @author: lydms
 * @create: 2023-06-07 00:53
 **/
@RestController
@RequestMapping("aReturn")
public class AReturnController extends BaseController {
    @Autowired
    private AReturnService aReturnService;
//    商品
    @Autowired
    private YGoodsService yGoodsService;
//    订单
    @Autowired
    private OrderService orderService;
//    收货人
    @Autowired
    private ConsigneeService consigneeService;

    @Autowired
    private ASaleImeiService aSaleImeiService;

//
    @PostMapping("aReturns")
    public AxiosResult<PageBean> AReturns(@RequestBody Map<String,String> map){
        QueryWrapper<AReturn> queryWrapper = new QueryWrapper<>();
        queryWrapper.like(map.get("orderId")!=null&&!"".equals(map.get("orderId")),"order_id",map.get("orderId"))
                .eq(map.get("recorder")!=null&&!"".equals(map.get("recorder")),"recorder",map.get("recorder"))
                .eq(map.get("auditPerson")!=null&&!"".equals(map.get("auditPerson")),"audit_person",map.get("auditPerson"))
                .eq(map.get("invoices")!=null&&!"".equals(map.get("invoices")),"invoices",map.get("invoices"))
                .between(map.get("recordTime")!=null&&map.get("recordTime1")!=null&&!"".equals(map.get("recordTime"))&&!"".equals(map.get("recordTime1")),"record_time",map.get("recordTime"), map.get("recordTime1"))
                .between(map.get("auditDate")!=null&&map.get("auditDate1")!=null&&!"".equals(map.get("auditDate"))&&!"".equals(map.get("auditDate1")),"audit_date",map.get("auditDate"),
                        map.get("auditDate1"));

        PageHelper.startPage(1, 5);
        List<AReturn> aReturns = aReturnService.search(queryWrapper);
        List<YGoods> imeis=new ArrayList<>();
        for (AReturn aReturn : aReturns) {
            aReturn.setbConsignee(consigneeService.findByOId(new LambdaQueryWrapper<BConsignee>().eq(BConsignee::getOrderId,aReturn.getOrderId())));
            aReturn.setOrder(orderService.search(new QueryWrapper<Order>().eq("order_id",aReturn.getOrderId())).get(0));
            List<ASaleImei> outCode = aSaleImeiService.search(new QueryWrapper<ASaleImei>().eq("out_code", aReturn.getOutCode()));
            for (ASaleImei aSaleImei : outCode) {
                List<YGoods> imei = yGoodsService.search(new QueryWrapper<YGoods>().eq("imei", aSaleImei.getImei()));
                System.out.println(imei);
                imeis.addAll(imei);
            }
            aReturn.setyGoods(imeis);
            imeis=new ArrayList<>();
        }
        PageInfo<AReturn> aReturnPageInfo = new PageInfo<>(aReturns);
        PageBean<AReturn> pageBean = PageBean.initData(aReturnPageInfo.getTotal(),aReturns);
        return AxiosResult.success(pageBean);
    }
    //    查询全部
    @GetMapping
    public AxiosResult<List<AReturn>> list() {
        return AxiosResult.success(aReturnService.list());
    }

    //  根据id查询
    @GetMapping("{id}")
    public AxiosResult<AReturn> findById(@PathVariable Long id) {
        return AxiosResult.success(aReturnService.findById(id));
    }
//添加
    @PostMapping
    public AxiosResult<Void> add(@RequestBody AReturn aReturn){
        return toAxios(aReturnService.add(aReturn));
    }
//    修改
    @PutMapping
    public AxiosResult<Void> update(@RequestBody AReturn aReturn){
        return toAxios(aReturnService.update(aReturn));
    }

    @DeleteMapping("{id}")
    public AxiosResult<Void> delete(@PathVariable long id){
        return toAxios(aReturnService.deleteById(id));
    }

}

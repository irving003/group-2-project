package com.javasm.controller.after;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.javasm.common.http.AxiosResult;
import com.javasm.common.page.PageBean;
import com.javasm.controller.base.BaseController;
import com.javasm.entity.after.AAudit;
import com.javasm.entity.after.ARepair;
import com.javasm.service.after.AAuditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("aAudit")
public class AAuditController extends BaseController {

    @Autowired
    private AAuditService aAuditService;
//条件查询
    @PostMapping ("aAudits")
    public AxiosResult<PageBean> AAudit(@RequestBody Map<String,String> map){
        QueryWrapper<AAudit> queryWrapper = new QueryWrapper<>();
        queryWrapper.like(map.get("schemeBy")!=null&&!"".equals(map.get("schemeBy")),"scheme_by",
                        map.get("schemeBy"))
                .eq(map.get("create_time")!=null&&!"".equals(map.get("create_time")),"create_time",map.get(
                "createTime"))
                .eq(map.get("auditStatus")!=null&&!"".equals(map.get("auditStatus")),"audit_status",map.get("auditStatus"))
                .like(map.get("auditer")!=null&&!"".equals(map.get("auditer")),"auditer",map.get("auditer"))
                .eq(map.get("auditTime")!=null&&!"".equals(map.get("auditTime")),"audit_time",map.get("auditTime"))
                .eq(map.get("auditDate")!=null&&!"".equals(map.get("auditDate")),"audit_date",map.get("auditDate"));
        PageHelper.startPage(Integer.parseInt(map.get("pageNum")), Integer.parseInt(map.get("pageSize")));
        List<AAudit> aAudits = aAuditService.search(queryWrapper);
        PageInfo<AAudit> aRepairPageInfo = new PageInfo<>(aAudits);
        PageBean<AAudit> pageBean = PageBean.initData(aRepairPageInfo.getTotal(),aAudits);
        return AxiosResult.success(pageBean);
    }
    /**
     * 查询所有
     */
    @GetMapping
    public AxiosResult<List<AAudit>> list(){
        return AxiosResult.success(aAuditService.list());
    }

    /**
     * 根据id查询一条数据
     */
    @GetMapping("{id}")
    public AxiosResult<AAudit> findById(@PathVariable Long id){
        return AxiosResult.success(aAuditService.findById(id));
    }

    /**
     * 添加申诉管理数据
     */
    @PostMapping
    public AxiosResult<Void> add(@RequestBody AAudit aAudit){
        return toAxios(aAuditService.add(aAudit));
    }

    /**
     * 修改申诉管理数据
     */
    @PutMapping
    public AxiosResult<Void> update(@RequestBody AAudit aAudit){
        return toAxios(aAuditService.update(aAudit));
    }

    /**
     * 删除一条数据
     */
    @DeleteMapping("{id}")
    public AxiosResult<Void> deleteById(@PathVariable Long id){
        return toAxios(aAuditService.deleteById(id));
    }


    /**
     * 删除多条数据
     */
    @DeleteMapping("batch/{ids}")
    public AxiosResult<Void> batchDeleteByIds(@PathVariable List<Long> ids){
        return toAxios(aAuditService.batchDeleteByIds(ids));
    }

    /**s
     * 特殊申诉管理条件查询
     */


}

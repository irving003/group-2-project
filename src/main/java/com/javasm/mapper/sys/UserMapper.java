package com.javasm.mapper.sys;

import com.javasm.entity.sys.SUser;
import com.javasm.mapper.base.MyMapper;



public interface UserMapper extends MyMapper<SUser> {
}

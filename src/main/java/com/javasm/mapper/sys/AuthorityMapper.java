package com.javasm.mapper.sys;

import com.javasm.entity.sys.SAuthority;
import com.javasm.entity.sys.SAuthorityOperation;
import com.javasm.entity.sys.SOperation;
import com.javasm.entity.sys.SUser;

import java.util.List;

public interface AuthorityMapper {
    List<SAuthority> findAll();

    //   通过ID查询可执行的操作
    List<SOperation> findAllOperation(Integer id);

    //    条件查询
    List<SAuthority> findBysAuthority(SAuthority sAuthority);

    //    通过操作内容获取编号条件查询
    SOperation findByOperation(SOperation operation);

    //    添加权限
    int add(SAuthority sAuthority);

    //    向关联表添加
    int addAuthorityOperation(SAuthorityOperation authorityOperation);

    //    修改权限
    int update(SAuthority sAuthority);

    //    删除权限
    int deleteAuthority(Long authorityId);

    //    删除权限操作关联表
    int deleteAuthorityOperation(Long authorityId);
    //    查询报表
    List<SUser> findReport(SUser user);
}

package com.javasm.mapper.sys;

import com.javasm.entity.sys.SAuthority;
import com.javasm.entity.sys.SRole;
import com.javasm.entity.sys.SRoleAuthority;

import java.util.List;

public interface RoleMapper {
    List<SRole> findAll();

    List<SAuthority> findAuthorityById(Integer id);

    List<SRole> findBysRole(SRole sRole);

    SAuthority findByAuthority(SAuthority authority);

    int add(SRole sRole);

    int addAuthority(SRoleAuthority sRoleAuthority);
//  修改角色
    int updatesRole(SRole sRole);
//  修改角色权限

//  删除角色和角色关联表权限
    int deleteRoleAuthority(Long id);
    int deleteRole(Long id);
}

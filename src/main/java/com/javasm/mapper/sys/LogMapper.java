package com.javasm.mapper.sys;

import com.javasm.entity.sys.SLog;
import com.javasm.mapper.base.MyMapper;

public interface LogMapper  extends MyMapper<SLog> {
}

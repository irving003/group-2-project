package com.javasm.mapper.ye;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.javasm.entity.ye.YSupplier;
import com.javasm.mapper.base.MyMapper;

import java.util.List;

public interface SupplierMapper extends MyMapper<YSupplier> {
    //供应商动态条件查询
    List<YSupplier> findYSupplier(YSupplier ySupplier);
}

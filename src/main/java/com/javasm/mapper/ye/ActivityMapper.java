package com.javasm.mapper.ye;

import com.javasm.entity.ye.YActivity;
import com.javasm.mapper.base.MyMapper;

import java.util.List;

public interface ActivityMapper extends MyMapper<YActivity> {
    // 动态条件查询
    List<YActivity> conditionQuery(YActivity activity);
}

package com.javasm.mapper.ye;

import com.javasm.entity.ye.YBinding;
import com.javasm.mapper.base.MyMapper;

import java.util.List;

public interface BindingMapper extends MyMapper<YBinding> {
    //动态条件查询
    List<YBinding> findYBinding(YBinding yBinding);
}

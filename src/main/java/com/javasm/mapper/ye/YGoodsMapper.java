package com.javasm.mapper.ye;

import com.javasm.entity.ye.YGoods;
import com.javasm.mapper.base.MyMapper;

import java.util.List;

public interface YGoodsMapper extends MyMapper<YGoods> {


    // 商品动态条件查询
    List<YGoods> findYGoods(YGoods yGoods);

    // 赠品转商品动态条件查询
    List<YGoods> findYGiftAndYGoods(YGoods yGoods);


}

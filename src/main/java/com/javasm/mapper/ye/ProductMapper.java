package com.javasm.mapper.ye;

import com.javasm.entity.ye.YProduct;
import com.javasm.mapper.base.MyMapper;

import java.util.List;

public interface ProductMapper extends MyMapper<YProduct> {
    //条件条件查询
    List<YProduct> findYProduct(YProduct yProduct);

}

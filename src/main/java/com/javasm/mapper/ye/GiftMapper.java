package com.javasm.mapper.ye;

import com.javasm.entity.ye.YGift;
import com.javasm.entity.ye.YGoods;
import com.javasm.mapper.base.MyMapper;

import java.util.List;

public interface GiftMapper extends MyMapper<YGift> {
    //动态条件查询
    List<YGift> findYGift(YGift yGift);

    // 商品转赠品动态条件查询
    List<YGift> findYGoodsAndYGift(YGift yGift);
}

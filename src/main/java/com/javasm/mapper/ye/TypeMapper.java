package com.javasm.mapper.ye;

import com.javasm.entity.ye.YType;
import com.javasm.mapper.base.MyMapper;

public interface TypeMapper extends MyMapper<YType> {
}

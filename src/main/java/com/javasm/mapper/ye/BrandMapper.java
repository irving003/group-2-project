package com.javasm.mapper.ye;

import com.javasm.entity.ye.Brand;
import com.javasm.mapper.base.MyMapper;

/**
 * @author: Author
 * @className: BrandMapper
 * @description:
 * @date: 2023/6/5 11:27
 * @since: 11
 */
public interface BrandMapper extends MyMapper<Brand> {

}

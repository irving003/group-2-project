package com.javasm.mapper.ye;

import com.javasm.entity.ye.YTariff;
import com.javasm.mapper.base.MyMapper;

import java.util.List;

public interface TariffMapper extends MyMapper<YTariff> {
    // 动态条件查询
    List<YTariff> findYTariff(YTariff yTariff);
}

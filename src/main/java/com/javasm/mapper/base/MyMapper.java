package com.javasm.mapper.base;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.github.yulichang.base.MPJBaseMapper;

/**
 * @author: Author
 * @className: MyMapper
 * @description:
 * @date: 2023/6/5 11:40
 * @since: 11
 */
public interface MyMapper<T> extends BaseMapper<T> {
}

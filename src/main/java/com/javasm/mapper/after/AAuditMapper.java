package com.javasm.mapper.after;

import com.javasm.entity.after.AAudit;
import com.javasm.mapper.base.MyMapper;

public interface AAuditMapper extends MyMapper<AAudit> {
}

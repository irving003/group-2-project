package com.javasm.mapper.after;

import com.javasm.entity.after.AReturn;
import com.javasm.mapper.base.MyMapper;

public interface AReturnMapper extends MyMapper<AReturn> {
}

package com.javasm.mapper.ku;

import com.javasm.entity.ku.KManage;
import com.javasm.mapper.base.MyMapper;

public interface KKManageMapper extends MyMapper<KManage> {
}

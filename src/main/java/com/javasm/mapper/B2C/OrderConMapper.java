package com.javasm.mapper.B2C;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.javasm.entity.B2C.BOrderCon;

public interface OrderConMapper extends BaseMapper<BOrderCon> {
}

package com.javasm.mapper.B2C;

import com.javasm.entity.B2C.BConsignee;
import com.javasm.mapper.base.MyMapper;

public interface ConsigneeMapper extends MyMapper<BConsignee> {
}

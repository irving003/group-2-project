package com.javasm.mapper.B2C;

import com.javasm.entity.B2C.Order;
import com.javasm.mapper.base.MyMapper;

public interface OrderMapper extends MyMapper<Order> {
}

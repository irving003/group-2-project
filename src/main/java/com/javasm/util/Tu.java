package com.javasm.util;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.LineCaptcha;
import com.javasm.common.config.LogConfig;
import com.javasm.common.http.AxiosResult;
import com.javasm.service.sys.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.Jedis;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("tu")
public class Tu {

    @Autowired
    private RedisTemplate redisTemplate;
    private Jedis jedis = JedisUtil.getJedis();
    @GetMapping
    @LogConfig(operationModule = "登录系统", description = "获取登录验证码")
    public AxiosResult<String> login(HttpServletResponse response) {
//        jedis.set("code", code);
        LineCaptcha lineCaptcha = CaptchaUtil.createLineCaptcha(200, 100, 4, 1000);
        lineCaptcha.createImage("1234");
        redisTemplate.delete("code");
        redisTemplate.opsForValue().set("code", lineCaptcha.getCode());
        redisTemplate.expire("code", 180, TimeUnit.SECONDS);
        System.out.println(redisTemplate.opsForValue().get("code"));
        try {
            lineCaptcha.write(response.getOutputStream());
            response.getOutputStream().close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return AxiosResult.success(lineCaptcha.getCode());
    }
}

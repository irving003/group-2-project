package com.javasm.util;


import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author: Author
 * @className: JedisUtil
 * @description:
 * @date: 2023/5/24 16:48
 * @since: 11
 */
// jedis连接池的工具类
public class JedisUtil {

    private static JedisPool jedisPool;

    static {
        // 当类加载时会执行一次

        // 从类路径下读取文件
        InputStream inputStream = JedisUtil.class.getClassLoader().getResourceAsStream("jedis.properties");

        Properties properties = new Properties();

        try {
            // 读取字节流数据
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 创建连接池配置对象
        JedisPoolConfig poolConfig = new JedisPoolConfig();
        // 设置最大的连接数量
        poolConfig.setMaxTotal(Integer.parseInt(properties.getProperty("maxTotal")));
        // 最大空闲数
        poolConfig.setMaxIdle(Integer.parseInt(properties.getProperty("maxIdle")));
        String host = properties.getProperty("host");
        int port = Integer.parseInt(properties.getProperty("port"));
        // 创建jedis连接池对象
        jedisPool = new JedisPool(poolConfig,host,port);
    }

    // 获取jedis对象
    public static Jedis getJedis(){
        Jedis jedis = jedisPool.getResource();
        return jedis;
    }

    // 归还jedis
    public static void close(Jedis jedis){
        if (jedis != null){
            jedis.close();
        }
    }
}

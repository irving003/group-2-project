// 项目的入口文件->做公共的设置操作
// 导入Vue
import { createApp } from 'vue'
// 引入样式文件
// import './style.css'
// 引入App组件(根组件)
import App from './App.vue'
//引入路由配置，插件会自动读取此文件夹下的index.js文件
import router from './router'
// 全局导入axios和qs
import axios from 'axios'
import qs from 'qs'

// 扩展到window对象上
window.axios = axios
window.qs = qs
window.router = router

// 全局导入图标

let app = createApp(App)
app.use(router)
app.mount('#app')
// createApp(App).mount('#app')

// 设置公共路径
axios.defaults.baseURL = 'http://192.168.159.129:10010'
// axios可以携带cookie
axios.defaults.withCredentials = true

axios.interceptors.response.use(
  function (resp) {
    // 对响应数据做点什么
    return resp
  },
  function (error) {
    // 对响应错误做点什么
    // 没有登录
    router.push('/')
    console.log(error)
    return Promise.reject(error)
  }
)

import * as ElementPlusIconsVue from '@element-plus/icons-vue'
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}

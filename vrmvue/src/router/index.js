import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    { path: '/', redirect: '/login' },
    { path: '/login', component: () => import('../components/Login.vue'), meta: { title: '登录' } },
    // { path: '/main', component: () => import('../components/Main.vue'), meta: { title: '测试' } }
    {
      path: '/main',
      component: () => import('../components/Main.vue'),
      meta: { title: '首页' },
      children: [
        { path: '/sale', component: () => import('../components/after/sale.vue'), meta: { title: '返厂出入库管理' } },
        { path: '/aRepair', component: () => import('../components/after/aRepair.vue'), meta: { title: '维修点' } },
        // { path: '/aReturn', component: () => import('../components/after/aReturn.vue'), meta: { title: '退换货管理' } },
        { path: '/aAudit', component: () => import('../components/after/aAudit.vue'), meta: { title: '退换货申诉记录管理' } },
        { path: '/orderMgr', component: () => import('../components/B2C/Order.vue'), meta: { title: '订单管理' } },
        { path: '/Issuemgr', component: () => import('../components/B2C/Issue.vue'), meta: { title: '出库单管理' } },
        { path: '/returnMgr', component: () => import('../components/B2C/Return.vue'), meta: { title: '销售退货管理' } },
        { path: '/shopOrder', component: () => import('../components/B2C/ShopOrder.vue'), meta: { title: '网店订单管理' } },
        { path: '/goods', component: () => import('../components/ye/goods.vue'), meta: { title: '商品管理' } },

        { path: '/KManage', component: () => import('../components/ku/KManage.vue'), meta: { title: '列表' } },
        { path: '/authority', component: () => import('../components/sys/authority.vue'), meta: { title: '权限' } },
        { path: '/authority/report', component: () => import('../components/sys/authorityReport.vue'), meta: { title: '权限报表' } },
        { path: '/log', component: () => import('../components/sys/log.vue'), meta: { title: '日志' } },
        { path: '/role', component: () => import('../components/sys/Role.vue'), meta: { title: '角色' } }
      ]
    }
  ]
})

export default router
